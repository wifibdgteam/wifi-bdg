-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2017 at 02:04 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_wifi`
--

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `kode_kecamatan` int(11) NOT NULL,
  `nama_kecamatan` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`kode_kecamatan`, `nama_kecamatan`) VALUES
(1, 'ANDIR'),
(2, 'ANTAPANI'),
(3, 'ARCAMANIK'),
(4, 'ASTANA ANYAR'),
(5, 'BABAKAN CIPARAY'),
(6, 'BANDUNG KIDUL'),
(7, 'BANDUNG KULON'),
(8, 'BANDUNG WETAN'),
(9, 'BATUNUNGGAL'),
(10, 'BOJONGLOA KALER'),
(11, 'BOJONGLOA KIDUL'),
(12, 'BUAHBATU'),
(13, 'CIBEUNYING KALER'),
(14, 'CIBEUNYING KIDUL'),
(15, 'CIBIRU'),
(16, 'CICENDO'),
(17, 'CIDADAP'),
(18, 'CINAMBO'),
(19, 'COBLONG'),
(20, 'GEDEBAGE'),
(21, 'KIARACONDONG'),
(22, 'LENGKONG'),
(23, 'MANDALAJATI'),
(24, 'PANYILEUKAN'),
(25, 'RANCASARI'),
(26, 'REGOL'),
(27, 'SUKAJADI'),
(28, 'SUKASARI'),
(29, 'SUMUR BANDUNG'),
(30, 'UJUNGBERUNG');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `kode_lokasi` int(11) NOT NULL,
  `nama_lokasi` varchar(50) DEFAULT NULL,
  `foto` text,
  `fasilitas` text,
  `kode_kecamatan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`kode_lokasi`, `nama_lokasi`, `foto`, `fasilitas`, `kode_kecamatan`) VALUES
(1, 'TERAS CIKAPUNDUNG', 'img/wifi/TERAS_CIKAPUNDUNG.jpg', 'Jembatan Merah, Amphitheater, Air Mancur, Kolam', 17),
(2, 'TAMAN PASUPATI', 'img/wifi/TAMAN_PASUPATI.jpg', 'Taman, Tempat Bermain Skateboard', 19),
(3, 'TERMINAL LEUWIPANJANG', 'img/wifi/TERMINAL_LEUWIPANJANG.jpg', 'Terminal Bis', 11),
(4, 'TAMAN KOTA CIKAPAYANG DAGO', NULL, NULL, 19),
(5, 'TAMAN FLEXI', NULL, NULL, 8),
(6, 'MASJID PUSDAI', NULL, NULL, 13),
(7, 'TAMAN ALUN ALUN UJUNG BERUNG', 'img/wifi/TAMAN_ALUN_ALUN_UJUNG_BERUNG.jpg', 'Taman, Sarana Ibadah', 30),
(8, 'MASJID RAYA UJUNG BERUNG', 'img/wifi/MASJID_RAYA_UJUNG_BERUNG.jpg', 'Sarana Ibadah, Taman', 30),
(9, 'RSUD KOTA BANDUNG', 'img/wifi/RSUD_KOTA_BANDUNG.jpg', 'Rumah Sakit, Apotik', 18),
(10, 'TAMAN MUSIK CENTRUM', NULL, NULL, 29),
(11, 'MASJID SALMAN ITB', 'img/wifi/MASJID_SALMAN_ITB.jpg', 'Sarana Ibadah', 19),
(12, 'PONPES DAARUTTAUBAH', NULL, NULL, 1),
(13, 'TAMAN CEMPAKA', NULL, NULL, 29),
(14, 'TAMAN SUPRATMAN', NULL, NULL, 8),
(15, 'TAMAN LANSIA', 'img/wifi/TAMAN_LANSIA.jpg', 'Taman', 8),
(16, 'MASJID RAYA CIPAGANTI', 'img/wifi/MASJID_RAYA_CIPAGANTI.jpg', 'Sarana Ibadah, Kajian Islam', 27),
(17, 'MUSEUM ASIA AFRIKA', NULL, NULL, 29),
(18, 'MUSEUM SRIBADUGA', NULL, NULL, 4),
(19, 'MASJID MUJAHIDIN', NULL, NULL, 22),
(20, 'MASJID DAARUT TAUHIID BANDUNG', 'img/wifi/MASJID_DAARUT_TAUHIID_BANDUNG.jpg', 'Sarana Ibadah Umat Islam', 28),
(21, 'TAMAN FITNES', 'img/wifi/TAMAN_FITNES.jpg', 'Taman, Sarana Olahraga', 19),
(22, 'TAMAN PUSTAKA BUNGA', NULL, NULL, 8),
(23, 'TAMAN TEGALLEGA', NULL, NULL, 26),
(24, 'TAMAN KELURAHAN LEBAK SILIWANGI', NULL, NULL, 19),
(25, 'MASJID AL MANAR', NULL, NULL, 19),
(26, 'MASJID AL MUROSALAH', NULL, NULL, NULL),
(27, 'MASJID AL UKHUWAH', NULL, NULL, 29),
(28, 'SANGGAR SENI', NULL, NULL, 19),
(29, 'TAMAN BIMA', NULL, NULL, 16),
(30, 'TAMAN ANGGREK SUPERHERO', NULL, NULL, 8),
(31, 'BANDARA HUSEIN', NULL, NULL, 16),
(32, 'TAMAN LALU LINTAS ADE IRMA SURYANI', NULL, NULL, 29),
(33, 'TAMAN MONUMEN PERJUANGAN RAKYAT', NULL, NULL, NULL),
(34, 'STASIUN BANDUNG', NULL, NULL, 1),
(35, 'GOR CITRA', NULL, NULL, 13),
(36, 'TAMAN PANATAYUDA', NULL, NULL, 19),
(37, 'TAMAN GASIBU', NULL, NULL, 8),
(38, 'STASIUN KIARACONDONG', NULL, NULL, 21),
(39, 'TERMINAL CICAHEUM', 'img/wifi/TERMINAL_CICAHEUM.jpg', 'Terminal Bis', 21),
(40, 'TAMAN GOR SAPARUA', NULL, NULL, NULL),
(41, 'TAMAN PRAMUKA', 'img/wifi/TAMAN_PRAMUKA.jpg', 'Taman', 8),
(42, 'PUBLIK AREA ARCAMANIK', NULL, NULL, 3),
(43, 'KEBUN BINATANG BANDUNG', NULL, NULL, 19),
(44, 'TAMAN SAUYUNAN BARAYA', NULL, NULL, 25),
(45, 'PET PARK', NULL, NULL, 8),
(46, 'LAPANG GASMIN ANTAPANI', NULL, NULL, 2),
(47, 'TAMAN TONGKENG', NULL, NULL, 29),
(48, 'TAMAN RESTORASI SUNGAI CIKAPAYANG', NULL, NULL, NULL),
(49, 'TAMAN VANDA', NULL, NULL, 29),
(50, 'CIKAPUNDUNG RIVER SPORT', NULL, NULL, 29),
(51, 'TAMAN CIBEUNYING', NULL, NULL, 8),
(52, 'MASJID ISTIQOMAH', NULL, NULL, 8),
(53, 'GEREJA KATEDRAL BANDUNG', NULL, NULL, 29),
(54, 'GEREJA HKBP', NULL, NULL, 7),
(55, 'BALAIKOTA BANDUNG', 'img/wifi/BALAIKOTA_BANDUNG.jpg', 'Taman Badak, Kolam air', 29),
(56, 'MASJID AGUNG BUAHBATU', NULL, NULL, 12),
(57, 'MASJID AL-JIHAD', NULL, NULL, 19),
(58, 'MASJID AL-ADLA', NULL, NULL, 3),
(59, 'MASJID AL-HIKMAH', NULL, NULL, 25),
(60, 'MASJID MIFTAHUL HIDAYAH', NULL, NULL, 8),
(61, 'BPPT', NULL, NULL, 9),
(62, 'MASJID AL-LATHIIF', NULL, NULL, 8),
(63, 'MASJID AL-HIDAYAH', NULL, NULL, 3),
(64, 'TAMAN PENDOPO WALIKOTA', NULL, 'Wisata Sejarah Kota Bandung', 29),
(65, 'TAMAN 3G', NULL, NULL, NULL),
(66, 'TAMAN ALUN ALUN BANDUNG', 'img/wifi/TAMAN_ALUN_ALUN_BANDUNG.jpg', 'Taman, Sarana Ibadah', 26);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(18) NOT NULL,
  `password` text NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`, `nama`) VALUES
('10113279', 'ee269a27a6d0fa75971381a140d18f70', 'Green Grandis'),
('10113287', '9f31c4091a36350c31e86f7940d035d9', 'Rian Gustandi'),
('10113310', '89525db465ea4dde7436e873fd64db93', 'Fajar Cahyo Prabowo'),
('admin', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `wifi`
--

CREATE TABLE `wifi` (
  `no` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `status` enum('AKTIF','NONAKTIF') NOT NULL,
  `kode_lokasi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wifi`
--

INSERT INTO `wifi` (`no`, `nama`, `latitude`, `longitude`, `status`, `kode_lokasi`) VALUES
(15, 'Bandung Juara TRC 03', '-6.88421924', '107.60729094', 'AKTIF', 1),
(23, 'Bandung Juara MDAT 02', '-6.86372799', '107.59003755', 'AKTIF', 20),
(25, 'Bandung Juara TJB 02', '-6.89809082', '107.60878906', 'AKTIF', 2),
(27, 'Bandung Juara TJB 01', '-6.89819454', '107.60913242', 'AKTIF', 2),
(28, 'Bandung Juara TJB 03', '-6.89821448', '107.60937403', 'AKTIF', 2),
(29, 'Bandung Juara TRC 01', '-6.88456839', '107.60716796', 'AKTIF', 1),
(30, 'Bandung Juara MSMN 02', '-6.89412248', '107.61092556', 'AKTIF', 11),
(31, 'Bandung Juara MSMN 01', '-6.89406911', '107.61119178', 'AKTIF', 11),
(32, 'Bandung Juara TTU 02', '-6.89185514', '107.61546957', 'AKTIF', 21),
(33, 'Bandung Juara TTU 01', '-6.89193187', '107.61598751', 'AKTIF', 21),
(35, 'Bandung Juara TTU 03', '-6.89189178', '107.61592327', 'AKTIF', 21),
(38, 'Bandung Juara TLN 01', '-6.90200926', '107.62046066', 'AKTIF', 15),
(39, 'Bandung Juara TLN 02', '-6.90183212', '107.62049467', 'AKTIF', 15),
(40, 'Bandung Juara TPM 01', '-6.91031332', '107.62703232', 'AKTIF', 41),
(41, 'Bandung Juara TPM 02', '-6.91004133', '107.62693912', 'AKTIF', 41),
(42, 'Bandung Juara TMP 03', '-6.9097219', '107.62720628', 'AKTIF', 41),
(43, 'Bandung Juara TMP 04', '-6.90975657', '107.62761651', 'AKTIF', 41),
(44, 'Bandung Juara TLW 01', '-6.94551242', '107.59387834', 'AKTIF', 3),
(45, 'Bandung Juara TLW 02', '-6.94650492', '107.59334239', 'AKTIF', 3),
(46, 'Bandung Juara TLW 03', '-6.94573878', '107.59298632', 'AKTIF', 3),
(47, 'Bandung Juara TCC 01', '-6.90238494', '107.6569706', 'AKTIF', 39),
(48, 'Bandung Juara TCC 02', '-6.90236407', '107.65697563', 'AKTIF', 39),
(49, 'Bandung Juara TCC 03', '-6.90241889', '107.65723683', 'AKTIF', 39),
(50, 'Bandung Juara RSUD 02', '-6.9157701', '107.69918629', 'AKTIF', 9),
(51, 'Bandung Juara RSUD 01', '-6.91577359', '107.69922469', 'AKTIF', 9),
(52, 'BANDUNG JUARA RSUD 03', '-6.91562178', '107.69931854', 'AKTIF', 9),
(53, 'Bandung Juara MRUB 02', '-6.91393256', '107.70086719', 'AKTIF', 8),
(54, 'Bandung Juara MRUB 04', '-6.91376845', '107.70114915', 'AKTIF', 8),
(55, 'Bandung Juara MRUB 03', '-6.91379154', '107.70112497', 'AKTIF', 8),
(56, 'Bandung Juara MRUB 01', '-6.91335785', '107.70087088', 'AKTIF', 8),
(57, 'Bandung Juara TAU 01', '-6.91408282', '107.70132543', 'AKTIF', 7),
(58, 'Bandung Juara TAU 02', '-6.91336294', '107.70154023', 'AKTIF', 7),
(59, 'Bandung Juara TAA 03', '-6.92111675', '107.60693451', 'AKTIF', 66),
(60, 'Bandung Juara TAA 01', '-6.92101814', '107.60728499', 'AKTIF', 66),
(61, 'Bandung Juara TAA 02', '-6.92233022', '107.60697716', 'AKTIF', 66),
(62, 'Bandung Juara TAA 04', '-6.92193061', '107.60725581', 'AKTIF', 66),
(63, 'Bandung Juara MRCP 04', '-6.89471177', '107.60242683', 'AKTIF', 16),
(64, 'Bandung Juara MRCP 02', '-6.89471397', '107.60241788', 'AKTIF', 16),
(72, 'BANDUNG JUARA BKB 01', '-6.91290705', '107.60975188', 'AKTIF', 55),
(73, 'BANDUNG JUARA BKB 02', '-6.91372691', '107.60950991', 'AKTIF', 55),
(74, 'BANDUNG JUARA BKB 03', '-6.91360410', '107.61013580', 'AKTIF', 55),
(75, 'BANDUNG JUARA BKB 04', '-6.91334874', '107.61022842', 'AKTIF', 55),
(76, 'BANDUNG JUARA BKB 05', '-6.91255526', '107.60969198', 'AKTIF', 55),
(77, 'BANDUNG JUARA BKB 06', '-6.91256058', '107.60989583', 'AKTIF', 55),
(78, 'BANDUNG JUARA MDAT 01', '-6.86363201', '107.58992964', 'AKTIF', 20),
(80, 'BANDUNG JUARA MDAT 03', '-6.86356342', '107.59004420', 'AKTIF', 20),
(81, 'BANDUNG JUARA MDAT 04', '-6.86347820', '107.58996374', 'AKTIF', 20),
(82, 'BANDUNG JUARA TJB 04', '-6.89806247', '107.60915228', 'AKTIF', 2),
(83, 'BANDUNG JUARA RSUD 04', '-6.91587731', '107.69854285', 'AKTIF', 9),
(84, 'BANDUNG JUARA TTU 04', '-6.89209790', '107.61545697', 'AKTIF', 21),
(85, 'BANDUNG JUARA MRCP 01', '-6.89455618', '107.60219847', 'AKTIF', 16),
(86, 'BANDUNG JUARA MRCP 03', '-6.89465736', '107.60224139', 'AKTIF', 16),
(88, 'BANDUNG JUARA TRC 02', '-6.88435633', '107.60727725', 'AKTIF', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`kode_kecamatan`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`kode_lokasi`),
  ADD KEY `lokasi_ibfk_1` (`kode_kecamatan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `wifi`
--
ALTER TABLE `wifi`
  ADD PRIMARY KEY (`no`),
  ADD UNIQUE KEY `latitude` (`latitude`),
  ADD UNIQUE KEY `longitude` (`longitude`),
  ADD UNIQUE KEY `nama` (`nama`),
  ADD KEY `wifi_ibfk_2` (`kode_lokasi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `kode_kecamatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `kode_lokasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `wifi`
--
ALTER TABLE `wifi`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD CONSTRAINT `lokasi_ibfk_1` FOREIGN KEY (`kode_kecamatan`) REFERENCES `kecamatan` (`kode_kecamatan`);

--
-- Constraints for table `wifi`
--
ALTER TABLE `wifi`
  ADD CONSTRAINT `wifi_ibfk_2` FOREIGN KEY (`kode_lokasi`) REFERENCES `lokasi` (`kode_lokasi`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
