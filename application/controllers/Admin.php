<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('LoginAdminModel');
	}

	function index() {
        $session = $this->session->userdata('isLoginAdmin'); //mengabil dari session apakah sudah login atau belum
        if($session == FALSE) //jika session false maka akan menampilkan halaman login
        {
            $this->load->view('login_admin');
        }else //jika session true maka di redirect ke halaman Data_pemilik_usaha
        {
            redirect('Wifi');
        }
    }

    function do_login() {
        $username = $this->input->post("uname");
        $password = md5($this->input->post("pass"));

        $this->form_validation->set_rules('uname','username','required|trim');
        $this->form_validation->set_rules('pass','password','required|trim');

        if($this->form_validation->run()==false) {
            $this->session->set_flashdata('fmessage','Username dan Password harus diisi !');
            redirect('admin');
        } else {
            $cek = $this->LoginAdminModel->cek_user($username,$password); //melakukan persamaan data dengan database
            if(count($cek) == 1){ //cek data berdasarkan username & pass
                foreach ($cek as $cek) {
                    $nama = $cek['nama']; //mengambil data(level/hak akses) dari database
                    $username = $cek['username']; 
                }                

                $this->session->set_userdata(array(
                    'isLoginAdmin'   => TRUE, //set data telah login
                    'uname'  => $username, //set session username
                    'name'      => $nama,
                ));
                    
                redirect('Wifi','refresh');  //redirect ke halaman Data_pemilik_usaha
            }else{ //jika data tidak ada yng sama dengan database
                $this->session->set_flashdata('fmessage','Username atau Password salah !');
                redirect('admin');
            } 
        }                
    }

    function logout() {
        $data = array('isLoginAdmin','uname','name');
        $this->session->unset_userdata($data);
        redirect('admin','refresh');
    }

}

?>