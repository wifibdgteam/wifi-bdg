<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Wifi_model');
        $this->load->model('Kecamatan_model');
        $this->load->library('form_validation');
    }

    function index() {
    	$this->template->load('user_template','user/home');
    }

    function tampil_marker(){
    	$kecamatan = $_POST['kecamatan'];
    	$data = array();
    	$kode_kecamatan = $this->Kecamatan_model->get_by_name($kecamatan);
    	if($kode_kecamatan){
    		$results = $this->Wifi_model->get_by_kecamatan($kode_kecamatan->kode_kecamatan);
    		if($results) {
	    		$data = array(
	    			'wifi_data' => $results, 
	    		);
	    	}else{
	    		$wifi = $this->Wifi_model->get_all();
	    		$data = array(
	    			'wifi_data' => $wifi 
	    		);
	    	}
    	}

    	echo json_encode($data);
    }

    function daftar_lokasi() {
    	$wifi = $this->Wifi_model->get_all_group();

    	$data = array(
    		'wifi_data' => $wifi, 
    	);

    	$this->template->load('user_template','user/daftar_lokasi',$data);
    }

    function lihat_detail($lokasi){
    	$wifi = $this->Wifi_model->get_by_kode_lokasi($lokasi);

    	$data = array(
    		'wifi_data' => $wifi,
    	);

    	$this->template->load('user_template','user/detail_lokasi',$data);
    }

    function tampil_seluruh_data() {
    	$wifi = $this->Wifi_model->get_active();
    	$data = array(
	    	'wifi_data' => $wifi 
	   	);

	   	echo json_encode($data);
    }

}