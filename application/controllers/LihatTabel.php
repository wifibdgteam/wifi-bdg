<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LihatTabel extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->auth->cek_auth();
		$this->load->model('Wifi_model');
        $this->load->library('form_validation');
	}

	public function index()
    {
        $wifi = $this->Wifi_model->get_all();

        $data = array(
            'wifi_data' => $wifi
        );

        $this->template->load('admin_template','admin/wifi/wifi_list', $data);
    }

}