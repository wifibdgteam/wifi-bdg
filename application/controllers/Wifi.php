<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wifi extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->auth->cek_auth();
		$this->load->model('Wifi_model');
        $this->load->model('kecamatan_model');
        $this->load->model('lokasi_model');
        $this->load->library('form_validation');
	}

	function index() {
        $this->breadcrumbs->push('Data Wifi', '/wifi');

        $wifi = $this->Wifi_model->get_all();

        $data = array(
            'wifi_data' => $wifi,
        );

        
		$this->template->load("admin_template","admin/beranda",$data);
	}

    function create() {
        $this->breadcrumbs->push('Data Wifi', '/wifi');
        $this->breadcrumbs->push('Tambah Data', '/wifi/create');
        $kecamatan = $this->kecamatan_model->get_all();
        $lokasi = $this->lokasi_model->get_all();

        $data_kecamatan = array(
            'kecamatan_data' => $kecamatan,
            'lokasi_data' => $lokasi,
        );

        $data = array(
            'button' => 'Tambah',
            'action' => site_url('wifi/create_action'),
            'no' => set_value('no'),
            'nama' => set_value('nama'),
            'latitude' => set_value('latitude'),
            'longitude' => set_value('longitude'),
            'fasilitas' => set_value('fasilitas'),
            'lokasi' => set_value('lokasi'),
            'status' => set_value('status'),
            'nama_kecamatan' => set_value('nama_kecamatan'),
        );


        $this->template->load('admin_template','admin/wifi/wifi_form', array_merge($data,$data_kecamatan));
    }

    function create_action(){
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'nama' => $this->input->post('nama',TRUE),
                'latitude' => $this->input->post('latitude',TRUE),
                'longitude' => $this->input->post('longitude',TRUE),
                'status' => $this->input->post('status',TRUE),
                'kode_lokasi' => $this->input->post('kode_lokasi',TRUE),
            );

            $this->Wifi_model->insert($data);
            $this->session->set_flashdata('message', 'Data '.$this->input->post('nama',TRUE).' telah ditambah');
            redirect(site_url('wifi'));
        }
    }

    public function update($id) 
    {
        $this->breadcrumbs->push('Data Wifi', '/wifi');
        $this->breadcrumbs->push('Edit Data', '/wifi/create');
        $row = $this->Wifi_model->get_by_id($id);
        $lokasi = $this->lokasi_model->get_all();

        if ($row) {
            $kecamatan = $this->Wifi_model->get_all();
            $data = array(
                'button' => 'Edit',
                'action' => site_url('wifi/update_action'),
                'lokasi_data' => $lokasi,

                'no' => set_value('no', $row->no),
                'nama' => set_value('nama',$row->nama),
                'latitude' => set_value('latitude',$row->latitude),
                'longitude' => set_value('longitude',$row->longitude),
                'fasilitas' => set_value('fasilitas',$row->fasilitas),
                'kode_lokasi' => set_value('lokasi',$row->kode_lokasi),
                'status' => set_value('status',$row->status),
                'nama_kecamatan' => set_value('nama_kecamatan',$row->nama_kecamatan),
            );
                $this->template->load('admin_template','admin/wifi/wifi_form', $data);
            } else {
                $this->session->set_flashdata('message', 'Record Not Found');
                redirect(site_url('lokasi'));
            }
    }

    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('no', TRUE));
        } else {
            $data = array(
                'nama' => $this->input->post('nama',TRUE),
                'latitude' => $this->input->post('latitude',TRUE),
                'longitude' => $this->input->post('longitude',TRUE),
                'status' => $this->input->post('status',TRUE),
            );

            $this->Wifi_model->update($this->input->post('no', TRUE), $data);
            $this->session->set_flashdata('message', 'Data '.$this->input->post('nama',TRUE).' telah dirubah');
            redirect(site_url('wifi'));
        }
    }

    public function delete($id) 
    {
        $row = $this->Wifi_model->get_by_id($id);

        if ($row) {
            $this->Wifi_model->delete($id);
            $this->session->set_flashdata('message', 'Data berhasil dihapus');
            redirect(site_url('wifi'));
        } else {
            $this->session->set_flashdata('message', 'Data tidak ditemukan');
            redirect(site_url('wifi'));
        }
    }

    // javascript call ---------------------------------------------------------------------------
    public function read_id()
    {
        $id = $_POST['no'];
        $row = $this->Wifi_model->get_by_id($id);
        if ($row) {
            $data = array(
                'no' => $row->no,
                'nama' => $row->nama,
                'lokasi' => $row->nama_lokasi,
                'fasilitas' => $row->fasilitas,
                'latitude' => $row->latitude,
                'longitude' => $row->longitude,
                'foto' => $row->foto,
            );
            echo json_encode($data);
        }    
    }

    public function hapus() 
    {
        $id = $_POST['no'];
        $row = $this->Wifi_model->get_by_id($id);

        if ($row) {
            unlink("./".$row->foto);
            $this->Wifi_model->delete($id);
            $this->session->set_flashdata('smessage', 'Data sukses dihapus');
            redirect(site_url('wifi'));
        } else {
            $this->session->set_flashdata('fmessage', 'Data tidak ditemukan');
            redirect(site_url('wifi'));
        }
    }

    public function hapus_form($id)
    {
        $row = $this->Wifi_model->get_by_id($id);

        if ($row) {
            unlink("./".$row->foto);
            $this->Wifi_model->delete($id);
            $this->session->set_flashdata('smessage', 'Data sukses dihapus');
            redirect(site_url('wifi'));
        } else {
            $this->session->set_flashdata('fmessage', 'Data tidak ditemukan');
            redirect(site_url('wifi'));
        }
    }


    function tampil_seluruh_data() {
        $wifi = $this->Wifi_model->get_all();
        $data = array(
            'wifi_data' => $wifi 
        );

        echo json_encode($data);
    }

    function tampil_wifi_aktif() {
        $wifi = $this->Wifi_model->get_active();
        $data = array(
            'wifi_data' => $wifi 
        );

        echo json_encode($data);
    }


    function get_data(){
        $kode_lokasi = $_POST['kode_lokasi'];
        $row = $this->lokasi_model->get_data($kode_lokasi);
        if($row){
            $data = array(
                'kode_lokasi' => $row->kode_lokasi,
                'nama_lokasi' => $row->nama_lokasi,
                'foto' => $row->foto,
                'fasilitas' => $row->fasilitas,
                'nama_kecamatan' => $row->nama_kecamatan, 
            );

            echo json_encode($data);
        }
    }

    public function _rules() 
    {
        $this->form_validation->set_rules('kode_lokasi', 'Lokasi', 'trim|required');
        $this->form_validation->set_rules('nama_kecamatan', 'Kecamatan', 'trim|required');
        $this->form_validation->set_rules('nama', 'Nama Wifi', 'trim|required');
        $this->form_validation->set_rules('latitude', 'Latitude', 'trim|required');
        $this->form_validation->set_rules('longitude', 'Longitude', 'trim|required');
        $this->form_validation->set_rules('status', 'Status', 'required');

        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
