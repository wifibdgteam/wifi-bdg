<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_list extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->auth->cek_auth();
        $this->load->model('Admin_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->breadcrumbs->push('Data Admin', '/admin_list');
        $user = $this->Admin_model->get_all();

        $data = array(
            'user_data' => $user
        );

        $this->template->load('admin_template','admin/user/admin_list', $data);
    }

    public function hapus($id)
    {
        $jumlah = $this->Admin_model->jumlah_data();
        if($jumlah->jumlah<=1) {
            $this->session->set_flashdata('fmessage', 'Data admin tidak boleh kosong');
            redirect(site_url('admin_list'));
        }else if($jumlah->jumlah>1){
            $row = $this->Admin_model->get_by_id($id);

            if ($row) {
                $this->Admin_model->delete($id);
                $this->session->set_flashdata('smessage', 'Data sukses dihapus');
                redirect(site_url('admin_list'));
            } else {
                $this->session->set_flashdata('fmessage', 'Data tidak ditemukan');
                redirect(site_url('admin_list'));
            }
        }
        
    }
}