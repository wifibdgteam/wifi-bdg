<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kecamatan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->auth->cek_auth();
        $this->load->model('kecamatan_model');
        $this->load->library('form_validation');

    }

    public function index()
    {
        $this->breadcrumbs->push('Data Kecamatan', '/kecamatan');
        $kecamatan = $this->kecamatan_model->get_all();

        $data = array(
            'kecamatan_data' => $kecamatan
        );

        $this->template->load('admin_template','admin/kecamatan/kecamatan_list', $data);
    }

    public function read() 
    {
        $id = $_POST['no'];
        $row = $this->kecamatan_model->get_by_id($id);
        if ($row) {
            $data = array(
        		'kode_kecamatan' => $row->kode_kecamatan,
        		'nama_kecamatan' => $row->nama_kecamatan,
    	    );
            echo json_encode($row);
        }
    }

    public function create() 
    {
        $this->breadcrumbs->push('Data Kecamatan', '/kecamatan');
        $this->breadcrumbs->push('Tambah Data', '/kecamatan/create');
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('kecamatan/create_action'),
    	    'no' => set_value('no'),
    	    'kode_kecamatan' => set_value('kode_kecamatan'),
    	    'nama_kecamatan' => set_value('nama_kecamatan'),
    	);
        $this->template->load('admin_template','admin/kecamatan/kecamatan_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
        		'nama_kecamatan' => $this->input->post('nama_kecamatan',TRUE),
    	    );

            $this->kecamatan_model->insert($data);
            $this->session->set_flashdata('smessage', 'Data'.$this->input->post('nama_kecamatan',TRUE).'telah ditambah');
            redirect(site_url('kecamatan'));
        }
    }
    
    public function update($id) 
    {
        $this->breadcrumbs->push('Data Kecamatan', '/kecamatan');
        $this->breadcrumbs->push('Edit Data', '/kecamatan/create');
        $row = $this->kecamatan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Edit',
                'action' => site_url('kecamatan/update_action'),
        		'kode_kecamatan' => set_value('kode_kecamatan', $row->kode_kecamatan),
        		'nama_kecamatan' => set_value('nama_kecamatan', $row->nama_kecamatan),
    	    );
            $this->template->load('admin_template','admin/kecamatan/kecamatan_form', $data);
        } else {
            $this->session->set_flashdata('fmessage', 'Data tidak ditemukan');
            redirect(site_url('kecamatan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_kecamatan', TRUE));
        } else {
            $data = array(
        		'nama_kecamatan' => $this->input->post('nama_kecamatan',TRUE),
    	    );

            $this->kecamatan_model->update($this->input->post('kode_kecamatan', TRUE), $data);
            $this->session->set_flashdata('smessage', 'Data '.$this->input->post('nama_kecamatan',TRUE).'telah dirubah');
            redirect(site_url('kecamatan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->kecamatan_model->get_by_id($id);

        if ($row) {
            $this->kecamatan_model->delete($id);
            $this->session->set_flashdata('smessage', 'Data berhasil dihapus');
            redirect(site_url('kecamatan'));
        } else {
            $this->session->set_flashdata('fmessage', 'Data tidak ditemukan');
            redirect(site_url('kecamatan'));
        }
    }

    public function _rules() 
    {
    	$this->form_validation->set_rules('nama_kecamatan', 'nama kecamatan', 'trim|required');

    	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "kecamatan.xls";
        $judul = "kecamatan";
        $tablehead = 1;
        $tablebody = 2;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel(0, 0, "Data kecamatan");
        xlsWriteLabel($tablehead, $kolomhead++, "No");
    	xlsWriteLabel($tablehead, $kolomhead++, "Kode kecamatan");
    	xlsWriteLabel($tablehead, $kolomhead++, "Nama kecamatan");

	   foreach ($this->kecamatan_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
    	    xlsWriteLabel($tablebody, $kolombody++, $data->kode_kecamatan);
    	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_kecamatan);

	        $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    function pdf () {
        $this->load->library('pdfgenerator');
        
        $kecamatan = $this->kecamatan_model->get_all();
        $data = array(
            'kecamatan_data' => $kecamatan,
        );

        
 
        $html = $this->load->view('admin/kecamatan/kecamatan_pdf', $data, true);
        //$this->load->view('admin/kecamatan/kecamatan_pdf', $data);
        
        $this->pdfgenerator->generate($html,'Data Kecamatan');
    }

}