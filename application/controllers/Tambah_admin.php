<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tambah_admin extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
        $this->auth->cek_auth();
		$this->load->model('Admin_model');
        $this->load->library('form_validation');
	}

	public function index() 
    {
        $this->breadcrumbs->push('Data Admin', '/admin_list');
        $this->breadcrumbs->push('Tambah Data', '/tambah_admin');
    	$data = array(
            'button' => 'Tambah',
            'action' => site_url('tambah_admin/aksi'),
            'nama' => set_value('nama'),
            'username' => set_value('username'),
            'password' => set_value('password'),
            //'level' => set_value('level'),
        );


        $this->template->load('admin_template','admin/user/admin_form', $data);
    }

    public function aksi() {

    	$this->_rules();

        if (($this->form_validation->run() == FALSE)) {
            $this->index();
        } else {

	        $data = array(
	        	'username' => $this->input->post('username',TRUE),
	        	'password' => md5($this->input->post('password',TRUE)) ,
	        	'nama' => $this->input->post('nama',TRUE),
	        	//'level' => $this->input->post('level',TRUE),
	    	);

	        $this->Admin_model->insert($data);
	        $error = $this->db->error();
	        if(!empty($error['message'])) {
                $this->session->set_flashdata('message', $error['message']);
                $this->index();
            } else {
                $this->session->set_flashdata('message', 'Admin baru dengan nama '.$data['nama'].' telah didaftarkan');
	          	redirect(site_url('admin_list'));
            }	            
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('nama', 'nama', 'trim|required');
		$this->form_validation->set_rules('username', 'username', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required|matches[konfirmasi]');
		$this->form_validation->set_rules('konfirmasi', 'konfirmasi', 'trim|required');
		//$this->form_validation->set_rules('level', 'level', 'trim|required');
		$this->form_validation->set_error_delimiters('<span class="text-danger"><i class="fa fa-times-circle-o"> ', '</i></span>');
    }

}

?>