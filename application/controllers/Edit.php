<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edit extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->auth->cek_auth();
		$this->load->model('Wifi_model');
        $this->load->model('kecamatan_model');
        $this->load->library('form_validation');
	}

	function index() {
		$id = $this->input->post('no',true);
		$row = $this->Wifi_model->get_by_id($id);

        $kecamatan = $this->kecamatan_model->get_all();

        $data_kecamatan = array(
            'kecamatan_data' => $kecamatan
        );

        if ($row) {
            $data = array(
                'button' => 'Edit',
                'action' => site_url('edit/aksi_edit'),
				'no' => set_value('no', $row->no),
				'nama' => set_value('nama', $row->nama),
				'latitude' => set_value('latitude', $row->latitude),
				'longitude' => set_value('longitude', $row->longitude),
				'detail' => set_value('detail', $row->detail),
				'lokasi' => set_value('lokasi', $row->lokasi),
                'foto' => set_value('foto', $row->foto),
				'status' => set_value('status', $row->status),
                'kode_kecamatan' => set_value('kode_kecamatan', $row->kode_kecamatan),
		    );

            $config['trafficOverlay'] = TRUE;
            $config['map_height'] = '600px';
            $config['zoom'] = '17';
            $config['center'] = $row->latitude.','.$row->longitude;
        	$this->googlemaps->initialize($config);

        	$keterangan  = '<b>Keterangan : </b><br>';
            $keterangan .= "<div class='row'>";
            $keterangan .= "<div class='col-sm-3'>";
            if($row->foto==''){
                $keterangan .= "<img src='".base_url('img/wifi/dummy.png')."' class='img-responsive' width='75px'>";
            }else{
                $keterangan .= "<img src='".base_url($row->foto)."' class='img-responsive' width='75px'>";
            }
            $keterangan .= "</div>";
            $keterangan .= "<div class='col-sm-9'>";
            $keterangan .= '<b>Nama WiFi : </b>'.$row->nama.'<br>';
            $keterangan .= '<b>Lokasi : </b>'.$row->lokasi.'<br>';
            $keterangan .= '<b>Fasilitas : </b>'.$row->detail.'<br>';
            $keterangan .= "</div>";
            $keterangan .= "</div>";

            $marker = array();
            $marker['position'] = $row->latitude.','.$row->longitude;
            $marker['infowindow_content'] = $keterangan;
            $marker['animation'] = 'DROP';
            $marker['draggable'] = true;
            $marker['ondragend'] = 'ordinat(event.latLng.lat(),event.latLng.lng());';
            $this->googlemaps->add_marker($marker);

            $peta['map'] = $this->googlemaps->create_map();

            $this->template->load('admin_template','admin/wifi/wifi_form',array_merge($data,$peta,$data_kecamatan));
        } else {
            $this->session->set_flashdata('fmessage', 'Data tidak ditemukan');
            redirect(site_url('admin'));
        }
	}

    public function form($id) {
        $row = $this->Wifi_model->get_by_id($id);
        $kecamatan = $this->kecamatan_model->get_all();

        $data_kecamatan = array(
            'kecamatan_data' => $kecamatan
        );

        if ($row) {
            $data = array(
                'button' => 'Edit',
                'action' => site_url('edit/aksi_edit'),
                'no' => set_value('no', $row->no),
                'nama' => set_value('nama', $row->nama),
                'latitude' => set_value('latitude', $row->latitude),
                'longitude' => set_value('longitude', $row->longitude),
                'detail' => set_value('detail', $row->detail),
                'lokasi' => set_value('lokasi', $row->lokasi),
                'foto' => set_value('foto', $row->foto),
                'status' => set_value('status', $row->status),
                'kode_kecamatan' => set_value('kode_kecamatan', $row->kode_kecamatan),
            );

            $config['trafficOverlay'] = TRUE;
            $config['map_height'] = '600px';
            $config['zoom'] = '17';
            $config['center'] = $row->latitude.','.$row->longitude;
            $this->googlemaps->initialize($config);

            $keterangan  = '<b>Keterangan : </b><br>';
            $keterangan .= "<div class='row'>";
            $keterangan .= "<div class='col-sm-3'>";
            if($row->foto==''){
                $keterangan .= "<img src='".base_url('img/wifi/dummy.png')."' class='img-responsive' width='75px'>";
            }else{
                $keterangan .= "<img src='".base_url($row->foto)."' class='img-responsive' width='75px'>";
            }
            $keterangan .= "</div>";
            $keterangan .= "<div class='col-sm-9'>";
            $keterangan .= '<b>Nama WiFi : </b>'.$row->nama.'<br>';
            $keterangan .= '<b>Lokasi : </b>'.$row->lokasi.'<br>';
            $keterangan .= '<b>Fasilitas : </b>'.$row->detail.'<br>';
            $keterangan .= "</div>";
            $keterangan .= "</div>";

            $marker = array();
            $marker['position'] = $row->latitude.','.$row->longitude;
            $marker['infowindow_content'] = $keterangan;
            $marker['animation'] = 'DROP';
            $marker['draggable'] = true;
            $marker['ondragend'] = 'ordinat(event.latLng.lat(),event.latLng.lng());';
            $this->googlemaps->add_marker($marker);

            $peta['map'] = $this->googlemaps->create_map();

            $this->template->load('admin_template','admin/wifi/wifi_form',array_merge($data,$peta,$data_kecamatan));
        } else {
            $this->session->set_flashdata('fmessage', 'Data tidak ditemukan');
            redirect(site_url('admin'));
        }
    }

	public function aksi_edit() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->index();           
        } else {

            $path = '';
            $config['upload_path']          = './img/wifi/';
            $config['allowed_types']        = 'jpg|jpeg';
            $config['file_name']            = $this->input->post('nama',TRUE);
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('foto')) {

                $this->session->set_flashdata('smessage', 'Data WiFi '.$this->input->post('nama',TRUE).' berhasil di ubah');
                $data = array(
                    'nama' => $this->input->post('nama',TRUE),
                    'latitude' => $this->input->post('latitude',TRUE),
                    'longitude' => $this->input->post('longitude',TRUE),
                    'detail' => $this->input->post('detail',TRUE),
                    'lokasi' => $this->input->post('lokasi',TRUE),
                    'status' => $this->input->post('status',TRUE),
                    'kode_kecamatan' => $this->input->post('kode_kecamatan',TRUE),
                );

                $this->Wifi_model->update($this->input->post('no', TRUE), $data);
                $error = $this->db->error();
                if(!empty($error['message'])) {
                    $this->session->set_flashdata('fmessage', $error['message']);
                    $this->index();
                } else {
                    $this->session->set_flashdata('smessage', 'Data WiFi '.$this->input->post('nama',TRUE).' berhasil di ubah');
                    redirect(site_url('admin'));
                }               

            } else {

                $row = $this->Wifi_model->get_by_id($this->input->post('no', TRUE));

                if ($row) {
                    $foto_lama = $row->foto;
                }

                $path = 'img/wifi/'.$this->upload->data('file_name');

                $data = array(
    				'nama' => $this->input->post('nama',TRUE),
    				'latitude' => $this->input->post('latitude',TRUE),
    				'longitude' => $this->input->post('longitude',TRUE),
    				'detail' => $this->input->post('detail',TRUE),
    				'lokasi' => $this->input->post('lokasi',TRUE),
                    'foto' => $path,
                    'status' => $this->input->post('status',TRUE),
                    'kode_kecamatan' => $this->input->post('kode_kecamatan',TRUE),

    		    );

                $this->Wifi_model->update($this->input->post('no', TRUE), $data);
                $error = $this->db->error();
                if(!empty($error['message'])) {
                    $this->session->set_flashdata('fmessage', $error['message']);
                    $this->index();
                } else {
                    unlink("./".$foto_lama);
                    $this->session->set_flashdata('smessage', 'Data WiFi '.$this->input->post('nama',TRUE).' berhasil di ubah');
                    redirect(site_url('admin'));
                }               
                
            }
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('nama', 'nama', 'trim|required');
		$this->form_validation->set_rules('latitude', 'latitude', 'trim|required');
		$this->form_validation->set_rules('longitude', 'longitude', 'trim|required');
		$this->form_validation->set_rules('detail', 'detail', 'trim|required');
		$this->form_validation->set_rules('lokasi', 'lokasi', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');
        $this->form_validation->set_rules('kode_kecamatan', 'kode_kecamatan', 'trim|required');
		$this->form_validation->set_rules('no', 'no', 'trim');
		$this->form_validation->set_error_delimiters('<i class="fa fa-times-circle-o text-danger">', '</i>');
    }
}