<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lokasi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Lokasi_model');
        $this->load->library('form_validation');
        $this->load->model('kecamatan_model');
        $this->load->model('wifi_model');
        $this->auth->cek_auth();
    }

    public function index()
    {
        $this->breadcrumbs->push('Data Lokasi', '/lokasi');
        $lokasi = $this->Lokasi_model->get_all();

        $data = array(
            'lokasi_data' => $lokasi
        );

        $this->template->load('admin_template','admin/lokasi/lokasi_list', $data);
    }

    public function read() 
    {
        $id = $_POST['kode_lokasi'];
        $row = $this->Lokasi_model->get_data($id);
        if ($row) {
            $data = array(
        		'kode_lokasi' => $row->kode_lokasi,
        		'nama_lokasi' => $row->nama_lokasi,
        		'foto' => $row->foto,
        		'fasilitas' => $row->fasilitas,
        		'nama_kecamatan' => $row->nama_kecamatan,
    	    );
            echo json_encode($data);
        }
    }

    function detail_lokasi($lokasi) {
        $this->breadcrumbs->push('Data Lokasi', '/lokasi');
        $this->breadcrumbs->push('Lihat Detail Lokasi', '/lokasi/detail_lokasi');
        $wifi = $this->Lokasi_model->get_by_kode_lokasi($lokasi);

        $data = array(
            'wifi_data' => $wifi,
        );

        $this->template->load("admin_template","admin/wifi/wifi_location",$data);
    }

    public function create() 
    {
        $this->breadcrumbs->push('Data Lokasi', '/lokasi');
        $this->breadcrumbs->push('Tambah Data', '/lokasi/create');
        $kecamatan = $this->kecamatan_model->get_all();
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('lokasi/create_action'),
            'kecamatan_data' => $kecamatan,
    	    'kode_lokasi' => set_value('kode_lokasi'),
    	    'nama_lokasi' => set_value('nama_lokasi'),
    	    'foto' => set_value('foto'),
    	    'fasilitas' => set_value('fasilitas'),
    	    'kode_kecamatan' => set_value('kode_kecamatan'),
    	);
            $this->template->load('admin_template','admin/lokasi/lokasi_form', $data);
        }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $path = '';
            $config['upload_path']          = './img/wifi/';
            $config['allowed_types']        = 'jpg|jpeg';
            $config['file_name']            = $this->input->post('nama_lokasi',TRUE);
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('foto')) {
                $this->session->set_flashdata('message', $this->upload->display_errors());
                $this->create();
            }else{
                $path = 'img/wifi/'.$this->upload->data('file_name');
                $data = array(
                    'nama_lokasi' => $this->input->post('nama_lokasi',TRUE),
                    'foto' => $path,
                    'fasilitas' => $this->input->post('fasilitas',TRUE),
                    'kode_kecamatan' => $this->input->post('kode_kecamatan',TRUE),
                );

                $this->Lokasi_model->insert($data);
                $error = $this->db->error();
                if(!empty($error['message'])) {
                    unlink("./".$path);
                    $this->session->set_flashdata('message', $error['message']);
                    $this->index();
                } else {
                    $this->session->set_flashdata('message', 'Create Record Success');
                    redirect(site_url('lokasi'));
                }
            }
            
        }
    }
    
    public function update($id) 
    {
        $this->breadcrumbs->push('Data Lokasi', '/lokasi');
        $this->breadcrumbs->push('Edit Data', '/lokasi/update');
        $row = $this->Lokasi_model->get_by_id($id);

        if ($row) {
            $kecamatan = $this->kecamatan_model->get_all();
            $data = array(
                'button' => 'Edit',
                'action' => site_url('lokasi/update_action'),
                'kecamatan_data' => $kecamatan,
        		'kode_lokasi' => set_value('kode_lokasi', $row->kode_lokasi),
        		'nama_lokasi' => set_value('nama_lokasi', $row->nama_lokasi),
        		'foto' => set_value('foto', $row->foto),
        		'fasilitas' => set_value('fasilitas', $row->fasilitas),
        		'kode_kecamatan' => set_value('kode_kecamatan', $row->kode_kecamatan),
    	    );
                $this->template->load('admin_template','admin/lokasi/lokasi_form', $data);
            } else {
                $this->session->set_flashdata('message', 'Record Not Found');
                redirect(site_url('lokasi'));
            }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_lokasi', TRUE));
        } else {
            $path = '';
            $config['upload_path']          = './img/wifi/';
            $config['allowed_types']        = 'jpg|jpeg';
            $config['file_name']            = $this->input->post('nama_lokasi',TRUE);
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('foto')) {
                $this->session->set_flashdata('message', $this->upload->display_errors());
                $data = array(
                    'nama_lokasi' => $this->input->post('nama_lokasi',TRUE),
                    'fasilitas' => $this->input->post('fasilitas',TRUE),
                    'kode_kecamatan' => $this->input->post('kode_kecamatan',TRUE),
                );
                $this->Lokasi_model->update($this->input->post('kode_lokasi', TRUE), $data);
                $error = $this->db->error();
                if(!empty($error['message'])) {
                    $this->session->set_flashdata('message', $error['message']);
                    $this->index();
                } else {
                    $this->session->set_flashdata('message', 'Update Record Success');
                    redirect(site_url('lokasi'));
                }
                
            }else {
                $path = 'img/wifi/'.$this->upload->data('file_name');
                $data = array(
                    'nama_lokasi' => $this->input->post('nama_lokasi',TRUE),
                    'foto' => $path,
                    'fasilitas' => $this->input->post('fasilitas',TRUE),
                    'kode_kecamatan' => $this->input->post('kode_kecamatan',TRUE),
                );

                $this->Lokasi_model->update($this->input->post('kode_lokasi', TRUE), $data);
                $error = $this->db->error();
                if(!empty($error['message'])) {
                    $this->session->set_flashdata('message', $error['message']);
                    $this->index();
                } else {
                    $this->session->set_flashdata('message', 'Update Record Success');
                    redirect(site_url('lokasi'));
                }
            }
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Lokasi_model->get_by_id($id);

        if ($row) {
            unlink("./".$row->foto);
            $this->Lokasi_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('lokasi'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('lokasi'));
        }
    }

    function pdf() {
        $this->load->library('pdfgenerator');
        
        $lokasi = $this->Lokasi_model->get_all();
        $data = array(
            'lokasi_data' => $lokasi,
        );

        
 
        $html = $this->load->view('admin/lokasi/lokasi_pdf', $data, true);
        //$this->load->view('admin/kecamatan/kecamatan_pdf', $data);
        
        $this->pdfgenerator->generate($html,'Data Lokasi');
    }

    public function _rules() 
    {
    	$this->form_validation->set_rules('nama_lokasi', 'nama lokasi', 'trim|required');
    	//$this->form_validation->set_rules('foto', 'foto', 'trim|required');
    	$this->form_validation->set_rules('fasilitas', 'fasilitas', 'trim|required');
    	$this->form_validation->set_rules('kode_kecamatan', 'kode kecamatan', 'trim|required');

    	$this->form_validation->set_rules('kode_lokasi', 'kode_lokasi', 'trim');
    	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "lokasi.xls";
        $judul = "lokasi";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
    	xlsWriteLabel($tablehead, $kolomhead++, "Nama Lokasi");
    	xlsWriteLabel($tablehead, $kolomhead++, "Foto");
    	xlsWriteLabel($tablehead, $kolomhead++, "Fasilitas");
    	xlsWriteLabel($tablehead, $kolomhead++, "Kode Kecamatan");

    	foreach ($this->Lokasi_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
    	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_lokasi);
    	    xlsWriteLabel($tablebody, $kolombody++, $data->foto);
    	    xlsWriteLabel($tablebody, $kolombody++, $data->fasilitas);
    	    xlsWriteNumber($tablebody, $kolombody++, $data->kode_kecamatan);

    	    $tablebody++;
                $nourut++;
        }

        xlsEOF();
        exit();
    }

}