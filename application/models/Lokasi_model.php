<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lokasi_model extends CI_Model
{

    public $table = 'lokasi';
    public $id = 'kode_lokasi';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $sql = "SELECT 
                    l.kode_lokasi, l.nama_lokasi, COUNT(w.no) jumlah_wifi, l.foto, l.fasilitas ,k.kode_kecamatan, k.nama_kecamatan
                FROM
                    lokasi l
                LEFT OUTER JOIN
                    kecamatan k ON l.kode_kecamatan=k.kode_kecamatan
                LEFT OUTER JOIN
                    wifi w ON l.kode_lokasi=w.kode_lokasi
                GROUP BY
                    l.kode_lokasi,l.nama_lokasi
                ORDER BY
                    l.kode_lokasi DESC";
        return $this->db->query($sql)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    function get_data($kode_lokasi) {
        $sql = "SELECT 
                    l.kode_lokasi, l.nama_lokasi, l.foto, l.fasilitas ,k.nama_kecamatan
                FROM
                    lokasi l
                LEFT OUTER JOIN 
                    kecamatan k ON l.kode_kecamatan=k.kode_kecamatan
                WHERE 
                    l.kode_lokasi=".$kode_lokasi;
        return $this->db->query($sql)->row();
    }

    function get_by_kode_lokasi($kode_lokasi)
    {
        $sql = "SELECT 
                    w.no , w.nama, w.latitude, w.longitude, l.fasilitas, w.status, l.kode_lokasi, l.nama_lokasi, l.foto, k.kode_kecamatan, k.nama_kecamatan
                FROM
                    wifi w 
                    RIGHT OUTER JOIN lokasi l 
                        ON w.kode_lokasi=l.kode_lokasi
                    LEFT OUTER JOIN kecamatan k
                        ON l.kode_kecamatan=k.kode_kecamatan
                WHERE l.kode_lokasi=".$kode_lokasi;
        return $this->db->query($sql)->result();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('kode_lokasi', $q);
	$this->db->or_like('nama_lokasi', $q);
	$this->db->or_like('foto', $q);
	$this->db->or_like('fasilitas', $q);
	$this->db->or_like('kode_kecamatan', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('kode_lokasi', $q);
	$this->db->or_like('nama_lokasi', $q);
	$this->db->or_like('foto', $q);
	$this->db->or_like('fasilitas', $q);
	$this->db->or_like('kode_kecamatan', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

