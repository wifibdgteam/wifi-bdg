<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wifi_model extends CI_Model
{

    public $table = 'wifi';
    public $id = 'no';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $sql = "SELECT 
                    w.no , w.nama, w.latitude, w.longitude, l.fasilitas, w.status, l.kode_lokasi, l.nama_lokasi, l.foto, k.kode_kecamatan, k.nama_kecamatan
                FROM
                    wifi w 
                    LEFT OUTER JOIN lokasi l 
                        ON w.kode_lokasi=l.kode_lokasi
                    LEFT OUTER JOIN kecamatan k
                        ON l.kode_kecamatan=k.kode_kecamatan
                        
                ORDER BY w.no DESC";
        return $this->db->query($sql)->result();
    }

    // get active
    function get_active()
    {
        $sql = "SELECT 
                    w.no , w.nama, w.latitude, w.longitude, l.fasilitas, w.status, l.kode_lokasi, l.nama_lokasi, l.foto, k.kode_kecamatan, k.nama_kecamatan
                FROM
                    wifi w 
                    LEFT OUTER JOIN lokasi l 
                        ON w.kode_lokasi=l.kode_lokasi
                    LEFT OUTER JOIN kecamatan k
                        ON l.kode_kecamatan=k.kode_kecamatan
                WHERE
                    w.status='AKTIF'    
                ORDER BY w.kode_lokasi";
        return $this->db->query($sql)->result();
    }

    // get active
    function get_nonactive()
    {
        $sql = "SELECT 
                    w.no , w.nama, w.latitude, w.longitude, l.fasilitas, w.status, l.kode_lokasi, l.nama_lokasi, l.foto, k.kode_kecamatan, k.nama_kecamatan
                FROM
                    wifi w 
                    LEFT OUTER JOIN lokasi l 
                        ON w.kode_lokasi=l.kode_lokasi
                    LEFT OUTER JOIN kecamatan k
                        ON l.kode_kecamatan=k.kode_kecamatan
                WHERE
                    w.status='NONAKTIF'    
                ORDER BY w.kode_lokasi";
        return $this->db->query($sql)->result();
    }

    function get_all_group()
    {
        $sql = "SELECT 
                    w.no , w.nama, w.latitude, w.longitude, l.fasilitas, w.status, l.kode_lokasi, l.nama_lokasi, l.foto, k.kode_kecamatan, k.nama_kecamatan
                FROM
                    wifi w 
                    LEFT OUTER JOIN lokasi l 
                        ON w.kode_lokasi=l.kode_lokasi
                    LEFT OUTER JOIN kecamatan k
                        ON l.kode_kecamatan=k.kode_kecamatan
                GROUP BY w.kode_lokasi";
        return $this->db->query($sql)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $sql = "SELECT 
                    w.no , w.nama, w.latitude, w.longitude, l.fasilitas, w.status, l.kode_lokasi, l.nama_lokasi, l.foto, k.kode_kecamatan, k.nama_kecamatan
                FROM
                    wifi w 
                    LEFT OUTER JOIN lokasi l 
                        ON w.kode_lokasi=l.kode_lokasi
                    LEFT OUTER JOIN kecamatan k
                        ON l.kode_kecamatan=k.kode_kecamatan
                WHERE w.no='".$id."'";
        return $this->db->query($sql)->row();
    }

    // get data by kode kecamatan
    function get_by_kecamatan($kode_kecamatan)
    {
        $sql = "SELECT 
                    w.no , w.nama, w.latitude, w.longitude, l.fasilitas, w.status, l.kode_lokasi, l.nama_lokasi, l.foto, k.kode_kecamatan, k.nama_kecamatan
                FROM
                    wifi w 
                    LEFT OUTER JOIN lokasi l 
                        ON w.kode_lokasi=l.kode_lokasi
                    LEFT OUTER JOIN kecamatan k
                        ON l.kode_kecamatan=k.kode_kecamatan
                WHERE k.kode_kecamatan='".$kode_kecamatan."' AND w.status='AKTIF'";
        return $this->db->query($sql)->result();
    }

    // get data by kode lokasi
    function get_by_lokasi($nama_lokasi)
    {
        $sql = "SELECT 
                    w.no , w.nama, w.latitude, w.longitude, l.fasilitas, w.status, l.kode_lokasi, l.nama_lokasi, l.foto, k.kode_kecamatan, k.nama_kecamatan
                FROM
                    wifi w 
                    LEFT OUTER JOIN lokasi l 
                        ON w.kode_lokasi=l.kode_lokasi
                    LEFT OUTER JOIN kecamatan k
                        ON l.kode_kecamatan=k.kode_kecamatan
                WHERE l.nama_lokasi='".$nama_lokasi."'";
        return $this->db->query($sql)->result();
    }

    // get data by kode lokasi
    function get_by_kode_lokasi($kode_lokasi)
    {
        $sql = "SELECT 
                    w.no , w.nama, w.latitude, w.longitude, l.fasilitas, w.status, l.kode_lokasi, l.nama_lokasi, l.foto, k.kode_kecamatan, k.nama_kecamatan
                FROM
                    wifi w 
                    LEFT OUTER JOIN lokasi l 
                        ON w.kode_lokasi=l.kode_lokasi
                    LEFT OUTER JOIN kecamatan k
                        ON l.kode_kecamatan=k.kode_kecamatan
                WHERE l.kode_lokasi=".$kode_lokasi;
        return $this->db->query($sql)->result();
    }

    // count data wifi unverified
    function get_count_unverified() {
        $this->db->where('verifikasi', 0);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get unverified
    function get_by_verificate()
    {
        $this->db->where('verifikasi', 0);
        return $this->db->get($this->table)->result();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('no', $q);
    	$this->db->or_like('nama', $q);
    	$this->db->or_like('latitude', $q);
    	$this->db->or_like('longitude', $q);
    	$this->db->or_like('detail', $q);
    	$this->db->or_like('lokasi', $q);
    	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('no', $q);
    	$this->db->or_like('nama', $q);
    	$this->db->or_like('latitude', $q);
    	$this->db->or_like('longitude', $q);
    	$this->db->or_like('detail', $q);
    	$this->db->or_like('lokasi', $q);
    	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}