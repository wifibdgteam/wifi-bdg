<?php
    $i = 1;
    foreach ($wifi_data as $wifi)
    {
        $nama_wifi[$i] = $wifi->nama;
        $latitude[$i] = $wifi->latitude;
        $longitude[$i] = $wifi->longitude;
        $foto = $wifi->foto;
        $fasilitas = $wifi->fasilitas;
        $lokasi = $wifi->nama_lokasi;
        $kecamatan = $wifi->nama_kecamatan;
        $i++;
    }
?>
<input type="hidden" id="lokasi" value="<?= $lokasi ?>">
<div class="row" style="padding-left: 20px; padding-right: 20px" onload="get_alamat('<?= $latitude[1] ?>')">
	<div class="col-lg-3">
		<h3></h3>
		<img src="<?= base_url($foto) ?>" class="img-responsive">
	</div>
	<div class="col-lg-9">
        <h3><b><?= "INFORMASI WIFI ".strtoupper($lokasi) ?></b></h3>
        <table class="table table-striped">
        	<tr>
        		<th>Jumlah Titik WiFi</th>
        		<td>: <?= $i-1 ?></td>
        	</tr>
        	<tr>
       			<th>Fasilitas</th>
       			<td>: <?= $fasilitas ?></td>
        	</tr>
        	<tr>
       			<th >Alamat</th>
       			<td><div id="alamat"></div> </td>
        	</tr>
        </table>

        <table class="table table-bordered table-striped">
        	<tr>
        		<th colspan="2"> DAFTAR TITIK WIFI DI <?= strtoupper($lokasi) ?> </th>
        	</tr>
        	<tr>
        		<th>Nama Wifi</th>
        		<th>Latitude, Longitude</th>
        	</tr>
        	<?php
        		for($j=1;$j<$i;$j++){
        	?>
        	<tr>
        		<td><?= strtoupper($nama_wifi[$j])  ?></td>
        		<td><?= $latitude[$j].','.$longitude[$j] ?></td>
        		
        	</tr>
        	<?php
        		}
        	?>
        	
        </table>
        <a href="<?= base_url('user/daftar_lokasi') ?>" class="btn btn-theme">Kembali</a>
	</div>
</div>

<script type="text/javascript">
	var lokasi = $('#lokasi').val();

	function get_alamat(lokasi){
		var url = "https://maps.googleapis.com/maps/api/geocode/json?address="+lokasi+"&components=country:ID&key=AIzaSyCE7HqUUvAXRZDYvvpZtJ8oB-ZgddhSPE4";
	    try {
	    	$.getJSON(url, function(data, textStatus){
	    		var alamat = data.results[0].formatted_address;
	    		$('#alamat').html(': '+alamat);
	    	})
	    }catch(e){
	    	alert(e);
	    }
	}

	window.onload = get_alamat(lokasi);
</script>