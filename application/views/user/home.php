<div class="row">
	<form method="post" id="frmcari">
		<div class="col-lg-1"></div>
		<div class="col-lg-9 ">
			<input type="text" class="form-control" name="txtcari" id="txtcari" placeholder="Cth. Kelurahan, Kecamatan">
		</div>
		<div class="col-lg-1">
			<input type="submit" class="btn" id="btncari" name="btncari" value="Cari Lokasi">
		</div>
	</form>
</div>
<br>
<div id="map"></div>

<script type="text/javascript">
	var map = new GMaps({
      el: '#map',
      lat: -6.914744,
      lng: 107.609810,
      zoom : 14
    });
    

	map.addControl({
	  position: 'left_top',
	  content: 'Tampilkan seluruh titik wifi',
	  style: {
	    margin : '5px 10px',
	    padding: '7px 5px',
	    height : '30px',
	   	border: 'solid 1px #717B87',
	    background: '#fff'
	  },
	  events: {
	    click: function(){
	     map.removeMarkers();
	     tampil_all_wifi();
	    }
	  }
	});

	map.addControl({
	  position: 'left_top',
	  content: 'Ke posisi saya',
	  style: {
	    margin : '5px 10px',
	    padding: '7px 5px',
	    height : '30px',
	   	border: 'solid 1px #717B87',
	    background: '#fff'
	  },
	  events: {
	    click: function(){
	     lokasi_saya();
	    }
	  }
	});

	map.addControl({
	  position: 'left_top',
	  content: 'Titik wifi di sekitar saya',
	  style: {
	    margin : '5px 10px',
	    padding: '7px 5px',
	    height : '30px',
	   	border: 'solid 1px #717B87',
	    background: '#fff'
	  },
	  events: {
	    click: function(){
	      map.removeMarkers();
	      geolocate();
	      map.setZoom(14);
	    }
	  }
	});

	$('#frmcari').submit(function(e){
        e.preventDefault();
        GMaps.geocode({
          address: $('#txtcari').val().trim(),
          callback: function(results, status){
            if(status=='OK'){
            	map.setZoom(15);
              	var latlng = results[0].geometry.location;
              	map.setCenter(latlng.lat(), latlng.lng());

            }
          }
        });
   	});

   	function geolocate(){
		GMaps.geolocate({
		  success: function(position) {
		    map.setCenter(position.coords.latitude, position.coords.longitude);
		    lokasi(position.coords.latitude, position.coords.longitude);
		    var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+position.coords.latitude+","+position.coords.longitude+"&sensor=true";
		    try {
		    	$.getJSON(url, function(data, textStatus){
		    		for (var i=0; i<data.results[0].address_components.length; i++) {
		    			for(var b=0; b<data.results[0].address_components[i].types.length; b++) {
		    				if (data.results[0].address_components[i].types[b] == "administrative_area_level_3") {
			                    var kecamatan = data.results[0].address_components[i].long_name.toUpperCase();
			                    break;
			                }
		    			}
		    		}
		    		//alert(kecamatan);
		    		set_marker(kecamatan);
		    	})
		    }catch(e){
		    	alert(e);
		    }
		  },
		  error: function(error) {
		    alert('Gagal mendapatkan lokasi: '+error.message);
		  },
		  not_supported: function() {
		    alert("Browser anda tidak support geolocation");
		  }
		});
	}

	function lokasi(lati,longi){
		map.addMarker({
		  lat: lati,
		  lng: longi,
		  title: 'Lokasi Anda',
		  icon: baseurl+"img/marker.png",
		  infoWindow: {
		      content: 'Lokasi Anda'
		    }
		});
	}

	function set_marker(kecamatan){
		$.ajax(
            {
                type: "POST",
                url: baseurl+"user/tampil_marker",
                data: {kecamatan:kecamatan},
                success: function(data){
                    try {
                    	var wifi = JSON.parse(data);
                    	for(var key in wifi.wifi_data){
                    		map.addMarker({
							  lat: wifi.wifi_data[key].latitude,
							  lng: wifi.wifi_data[key].longitude,
							  title: wifi.wifi_data[key].nama,
							  icon: baseurl+"img/wifi.png",
							  infoWindow: {
							  	content : "<b>Keterangan : </b><br><div class='row'><div class='col-sm-3'><img src='"+baseurl+wifi.wifi_data[key].foto+"' class='img-responsive' width='75px'></div><div class='col-sm-9'><b>Nama WiFi : </b>"+wifi.wifi_data[key].nama+"<br><b>Lokasi : </b>"+wifi.wifi_data[key].nama_lokasi+"<br><b>Fasilitas : </b>"+wifi.wifi_data[key].fasilitas+"</div></div>"
							  }
							});
                    	}                        
                        
                    } catch(e) {
                        console.log(e);
                    }
            	}
        	}
        );    
	}

	function lokasi_saya() {
		GMaps.geolocate({
		  success: function(position) {
		    map.setCenter(position.coords.latitude, position.coords.longitude);
		    map.setZoom(14);
		  },
		  error: function(error) {
		    alert('Gagal mendapatkan lokasi: '+error.message);
		  },
		  not_supported: function() {
		    alert("Browser anda tidak support geolocation");
		  }
		});
	}

	function tampil_all_wifi() {
		GMaps.geolocate({
		  success: function(position) {
		    map.setCenter(position.coords.latitude, position.coords.longitude);
		    lokasi(position.coords.latitude,position.coords.longitude);
		    map.setZoom(13);
		  },
		  error: function(error) {
		    alert('Gagal mendapatkan lokasi: '+error.message);
		  },
		  not_supported: function() {
		    alert("Browser anda tidak support geolocation");
		  }
		});

		var url = baseurl+"user/tampil_seluruh_data";
		try {
	    	$.getJSON(url, function(data, textStatus){
	    		for(var key in data.wifi_data) {
	    			map.addMarker({
						lat: data.wifi_data[key].latitude,
						lng: data.wifi_data[key].longitude,
						title: data.wifi_data[key].nama,
						icon: baseurl+"img/wifi.png",
						infoWindow: {
						  	content : "<b>Keterangan : </b><br><div class='row'><div class='col-sm-3'><img src='"+baseurl+data.wifi_data[key].foto+"' class='img-responsive' width='75px'></div><div class='col-sm-9'><b>Nama WiFi : </b>"+data.wifi_data[key].nama+"<br><b>Lokasi : </b>"+data.wifi_data[key].nama_lokasi+"<br><b>Fasilitas : </b>"+data.wifi_data[key].fasilitas+"</div></div>"
						}
					});
	    		}
	    	})
	    }catch(e){
	    	console.log(e);
	    }

	}

	window.open = geolocate();
	
</script>