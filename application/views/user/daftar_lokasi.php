<div style="padding-left: 20px; padding-right: 20px;">
<h3>DAFTAR LOKASI YANG SUDAH TERPASANG WIFI BANDUNG JUARA</h3>
<table class="table table-bordered table-striped" id="tabelku">
    <thead>
        <tr>
            <th width="10px">No</th>
            <th >Lokasi</th>
            <th width="80px">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $start = 1;
            foreach ($wifi_data as $wifi)
            {
        ?>
        <tr>
            <td><?= $start++ ?></td>
            <td><?php echo $wifi->nama_lokasi ?></td>
            <td style="text-align:center">
                <?php 
                    echo anchor(site_url('user/lihat_detail/'.$wifi->kode_lokasi),'Lihat Detail'); 
                ?>                                                                
            </td>
        </tr>
        <?php
            }
        ?>
    </tbody>
</table>
</div>

<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#tabelku").dataTable();
    });
</script>