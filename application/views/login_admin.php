<html>
    <head>
        <title>Login Admin SIG</title>
        <link rel="shortcut icon" href="<?php echo base_url('img/icon.ico') ?>">
        <?php $this->load->view('template_admin/css'); ?>

        <style type="text/css">
            html{
                overflow-y: hidden; 
            }

        </style>
    </head>

    <body class="hold-transition login-page">
        <div class="login-box">
            
            <div class="login-logo">
                <div class="row">
                    <div class="col-md-4">
                        <img class="img-responsive img-circle" src="<?= base_url('img/icon.png') ?>">
                    </div>
                    <div class="col-md-8" style="text-align: left;">
                        <h3>Login Admin SIG<br>WiFi Bandung Juara</h3>
                    </div>
                </div>
                
            </div>            
            <div class="login-box-body">
                <p class="login-box-msg">Silahkan Login ke Halaman Admin<br><strong>SIG WiFi Bandung Juara</strong></p>

                
                <form action="<?= base_url('admin/do_login'); ?>" method="post">
                    <div class="form-group has-feedback">
                        <label>Username</label>
                        <input type="text" name="uname" class="form-control" placeholder="" >
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <label>Password</label>
                        <input type="password" name="pass" class="form-control" placeholder="" >
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8"></div>
                        <div class="col-xs-4">
                            <button type="submit" name="admin" class="btn btn-primary btn-block btn-flat">
                                <i class="glyphicon glyphicon-log-out"></i> Login
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <?php
            if($this->session->flashdata('fmessage')) {
                $this->load->view('modals/fmodals.php');             
            }
        ?>

    <?php $this->load->view('template_admin/javascript'); ?>
    </body>
</html>