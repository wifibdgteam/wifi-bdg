<html>
<head>
	<?php $this->load->view('template_admin/css.php'); ?>
	<meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <link rel="shortcut icon" href="<?php echo base_url('img/icon.ico') ?>">
    
	<title>Admin SIG</title>

	<style type="text/css">
		.wrapper {
		  min-height: 100%;
		  position: relative;
		  overflow: hidden;
		  width: 100%;
		  min-height: 100%;
		  height: auto !important; 
		  position: absolute; 
		}

		
	</style>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCE7HqUUvAXRZDYvvpZtJ8oB-ZgddhSPE4&libraries=places"></script>
  	<script src="<?= base_url('vendor/solid/assets/js/gmaps.js') ?>"></script>
	
</head>
<body class="hold-transition skin-red " >

	<div style="background: white;">
		

		<div class="wrapper" style="background-color: #EBEDED;">
			<?php $this->load->view('template_admin/header.php'); ?>
			<?php $this->load->view('template_admin/sidebar.php'); ?>
			<?php $this->load->view('template_admin/content.php'); ?>
			<?php $this->load->view('template_admin/footer.php'); ?>
		</div>
		

			<?php 
			if($this->session->flashdata('smessage')) {
			    $this->load->view('modals/smodals.php');
			}
			if($this->session->flashdata('fmessage')) {
			    $this->load->view('modals/fmodals.php');
			}
		?>
	</div>

	<?php $this->load->view('template_admin/javascript.php'); ?>
</body>
</html>