
<section class="content">   

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs pull-right">
            <li >
                <a href="#list" data-toggle="tab">Lihat Dalam Daftar</a>
            </li>
            <li class="active">
                <a href="#lihatmap" data-toggle="tab">Lihat Dalam Map</a>
            </li>
            <li class="pull-left header">
                <i class="fa fa-wifi"></i>
                DATA WIFI
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane" id="list"> 
                <a href="#" class="btn btn-social btn-danger" title="Simpan ke PDF"><i class="fa fa-file-pdf-o"></i>Unduh ke PDF</a>
                <a href="#" class="btn btn-social btn-success" title="Simpan ke Excel"><i class="fa fa-file-excel-o"></i>Unduh ke Excel</a>
                <div class="pull-right">
                    <a href="<?= base_url('wifi/create') ?>" class="btn btn-social btn-primary" title="Tambah Data"><i class="fa fa-plus"></i>Tambah Data</a>
                </div>
                <br><br>
              
                <table class="table table-bordered table-striped" id="tabelku">
                    <thead>
                        <tr>
                            <th width="30px">No</th>
                            <th >Lokasi</th>
                            <th >Nama Wifi</th>
                            <th>Status</th>
                            <th style="text-align:center">Aksi</th>   
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $start = 1;
                            $spans = 1;
                            $lokasi = '';
                            foreach ($wifi_data as $wifi) {
                                if($lokasi==$wifi->nama_lokasi){
                                    $spans = $spans + 1;
                                }else{
                                    $spans=1;
                                }
                        ?>
                        <tr>
                            <td><?= $start++ ?></td>
                            <td><?= strtoupper($wifi->nama_lokasi) ?></td>
                            <td><?= strtoupper($wifi->nama) ?></td> 
                            <td><?= strtoupper($wifi->status) ?></td> 
                            <td style="text-align:center">
                                <button id="lihat" class="btn btn-danger btn-sm" onclick="info('<?= $wifi->no ?>')" title="Lihat Data"><i class="fa fa-eye"></i></button>
                                <?php 
                                echo '   '; 
                                echo anchor(site_url('wifi/update/'.$wifi->no),'<i class="fa fa-pencil"></i>','title="Edit Data" class="btn btn-danger btn-sm"'); 
                                echo '   '; 
                                echo anchor(site_url('wifi/delete/'.$wifi->no),'<i class="fa fa-trash"></i>','title="Hapus Data" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                                ?>
                            </td>   
                        </tr>
                        <?php
                                $lokasi = $wifi->nama_lokasi;
                            }
                        ?>
                    </tbody>
                </table>
            </div>
            
              <!-- /.tab-pane -->
            <div class="tab-pane active" id="lihatmap">
                <a href="#" class="btn btn-social btn-danger" title="Simpan ke PDF"><i class="fa fa-file-pdf-o"></i>Unduh ke PDF</a>
                <a href="#" class="btn btn-social btn-success" title="Simpan ke Excel"><i class="fa fa-file-excel-o"></i>Unduh ke Excel</a>
                <div class="pull-right">
                    <a href="<?= base_url('wifi/create') ?>" class="btn btn-social btn-primary" title="Tambah Data"><i class="fa fa-plus"></i>Tambah Data</a>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-sm-12">
                        <div id="map"></div><br>
                    </div>

                </div>  
            </div>
        </div>
    </div>

    
	
</section>

<?php
    //message box khusus
    $this->load->view('modals/alert/smsg_hapus.php');
    $this->load->view('modals/alert/msg_info.php');
    if($this->session->flashdata('message')){
        $this->load->view("modals/modals");
    }
?>

<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>

<script type="text/javascript">

    var baseurl = "<?php print base_url(); ?>";
    var map = new GMaps({
      el: '#map',
      lat: -6.914744,
      lng: 107.609810,
      height: '500px',
      width: '100%',
      fullscreenControl: true,
      zoom : 14
    });


    map.addControl({

      position: 'top_right',
      content: //"<div class='form-group has-feedback'>"
                    "<input type='text' name='txtcari' id='txtcari' class='form-control'  placeholder='Cari Lokasi'>",
                    //+"<span class='fa fa-search form-control-feedback'></span>"
                  //+"</div>",
      style: {
        margin: '10px',
      },
      events: {
        keypress: function(evt){
            $('#txtcari').keyup("keypress", function(e) {
                if (e.keyCode == 13) {
                    GMaps.geocode({
                      address: $('#txtcari').val().trim(),
                      callback: function(results, status){
                        if(status=='OK'){
                          var latlng = results[0].geometry.location;
                          map.setCenter(latlng.lat(), latlng.lng());
                        }
                      }
                    });
                }
            });
        }
      }
    });

    
    function tampil_wifi(url) {
        var bounds = new google.maps.LatLngBounds();
        try {
            $.getJSON(url, function(data, textStatus){
                for(var key in data.wifi_data) {
                    myLatLang = new google.maps.LatLng(data.wifi_data[key].latitude,data.wifi_data[key].longitude); 
                    bounds.extend(myLatLang);
                    map.addMarker({
                        lat: data.wifi_data[key].latitude,
                        lng: data.wifi_data[key].longitude,
                        title: data.wifi_data[key].nama,
                        icon: baseurl+"img/icon.ico",
                        //click : info(data.wifi_data[key].no),
                        infoWindow: {
                            content : "<b>Keterangan : </b><br>"
                            +"<div class='row'>"
                                +"<div class='col-sm-3'>"
                                    +"<img src='"+baseurl+data.wifi_data[key].foto+"' class='img-responsive' width='75px'>"
                                +"</div>"
                                +"<div class='col-sm-9'>"
                                    +"<b>Nama WiFi : </b>"+data.wifi_data[key].nama+"<br>"
                                    +"<b>Lokasi : </b>"+data.wifi_data[key].nama_lokasi+"<br>"
                                    +"<b>Status : </b>"+data.wifi_data[key].status+"<br><br>"
                                    +"<div class='row'>"
                                        +"<div class='col-sm-12'>"
                                            +"<button onclick=info("+data.wifi_data[key].no+") class='btn btn-default btn-xs'>Lihat Detail</button>"
                                            +"<div class='pull-right'>"
                                                +"<a href='"+baseurl+"wifi/update/"+data.wifi_data[key].no+"' class='btn btn-default btn-xs'>Edit Data</a><br>"
                                            +"</div>"
                                        +"</div>"
                                    +"</div>"
                                +"</div>"
                            +"</div>"
                            +"</br>"

                        }
                    });
                }
            })
            //map.fitBounds(bounds);
        }catch(e){
            console.log(e);
        }

    }

    

    $(document).ready(function () {
        $("#tabelku").dataTable();
        tampil_wifi(baseurl+"wifi/tampil_wifi_aktif");

    });




	function keterangan(no) {
        $.ajax(
            {
             type: "POST",
             url: baseurl+"wifi/read_id",
             data: {no:no},
             success: function(data){
                try {
                    var wifi = JSON.parse(data);
                    $('#no').html(wifi.no);
                    $('#nomor_edit').val(wifi.no);
                    $('#nama').html(wifi.nama);
                    $('#lokasi').html(wifi.lokasi);
                    $('#latlong').html(wifi.latitude+','+wifi.longitude);
                    $('#fasilitas').html(wifi.fasilitas);
                } catch(e) {
                    
                }
            }
        });
	}

    function hapus() {
        var yes = confirm('Apakah anda yakin ingin menghapus data ini ?');
        var no = $('#nomor_edit').val();
        if(yes){
            $.ajax(
                {
                 type: "POST",
                 url: baseurl+"wifi/hapus",
                 data: {no:no},
                 success: function(data){
                    try {
                        
                        $('#smsg_hapus').modal({
                            backdrop: 'static',
                            keyboard: false
                        });

                        
                    } catch(e) {
                        alert(e);
                    }
                }
            });
        }
    }

    function info(no) {
        $.ajax(
            {
             type: "POST",
             url: baseurl+"wifi/read_id",
             data: {no:no},
             success: function(data){
                try {
                    var wifi = JSON.parse(data);
                    $('#foto_alert').attr("src",baseurl+wifi.foto);
                    $('#no_alert').html('No WiFi : '+wifi.no);
                    $('#nomor_edit').val(wifi.no);
                    $('#nama_alert1').html('<i class="icon fa fa-wifi"></i> Info WiFi '+wifi.nama);
                    $('#nama_alert2').html('Nama WiFi : '+wifi.nama);
                    $('#lokasi_alert').html('Lokasi : '+wifi.lokasi);
                    $('#latlong_alert').html('Ordinat : '+wifi.latitude+','+wifi.longitude);
                    $('#fasilitas_alert').html('Fasilitas : '+wifi.fasilitas);
                    $('#msg_info').modal({
                        backdrop: 'static',
                        keyboard: false
                    });

                } catch(e) {
                    console.log(e);
                }
            }
        });    
    }

</script>
