<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <div class="row">
                <div class="col-md-8">
                    <h2 class="box-title">Data Kecamatan</h2>
                </div>
                <div class="col-md-4 text-right">
                    <?php echo anchor(site_url('kecamatan/pdf'), '<i class="fa fa-file-pdf-o"></i>', 'class="btn btn-social-icon btn-danger"'); ?>
                    <?php echo anchor(site_url('kecamatan/excel'), '<i class="fa fa-file-excel-o"></i>', 'class="btn btn-social-icon btn-success"'); ?>
                    <?php echo anchor(site_url('kecamatan/create'), '<i class="fa fa-plus"></i>Tambah Kecamatan', 'class="btn btn-social btn-primary"'); ?>
        	    </div>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped" id="tabelku">
                <thead>
                    <tr>
                        <th width="80px">No</th>
            		    <th>Nama Kecamatan</th>
            		    <th>Action</th>
                    </tr>
                </thead>
        	    <tbody>
                    <?php
                    $start = 0;
                    foreach ($kecamatan_data as $kecamatan)
                    {
                        ?>
                        <tr>
                		    <td><?php echo ++$start ?></td>
                		    <td><?php echo $kecamatan->nama_kecamatan ?></td>
                		    <td style="text-align:center" width="200px">
                            <button id="lihat" class="btn btn-danger btn-sm" onclick="info('<?= $kecamatan->kode_kecamatan ?>')" title="Lihat Data"><i class="fa fa-eye"></i></button>
                			<?php 
                			echo '   '; 
                			echo anchor(site_url('kecamatan/update/'.$kecamatan->kode_kecamatan),'<i class="fa fa-pencil"></i>','title="Edit Data" class="btn btn-danger btn-sm"'); 
                			echo '   '; 
                			echo anchor(site_url('kecamatan/delete/'.$kecamatan->kode_kecamatan),'<i class="fa fa-trash"></i>','title="Hapus Data" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                			?>
                		    </td>
        	           </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php 
                $this->load->view('admin/kecamatan/kecamatan_read');
            ?>


            <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
            <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
            <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $("#tabelku").dataTable();
                });

                function info(no) {
                    $.ajax(
                        {
                         type: "POST",
                         url: baseurl+"kecamatan/read",
                         data: {no:no},
                         success: function(data){
                            try {
                                var kecamatan = JSON.parse(data);

                                $('#kode_kecamatan_alert').html('Kode kecamatan : '+kecamatan.kode_kecamatan);
                                $('#nama_kecamatan_alert').html('Nama kecamatan : '+kecamatan.nama_kecamatan);
                                $('#msg_info').modal({
                                    backdrop: 'static',
                                    keyboard: false
                                });

                            } catch(e) {
                                alert(e);
                            }
                        }
                    });    
                }
            </script>
        </div>
    </div>
</section>
