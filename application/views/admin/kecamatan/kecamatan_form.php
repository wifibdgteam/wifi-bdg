<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h2 style="margin-top:0px"><?php echo $button ?> Kecamatan</h2>
        </div>
        <div class="box-body">
            <form action="<?php echo $action; ?>" method="post">
        	   
        	    <div class="form-group">
                    <label for="varchar">Nama kecamatan </label>
                    <input type="text" class="form-control" name="nama_kecamatan" id="nama_kecamatan" placeholder="Nama kecamatan" value="<?php echo $nama_kecamatan; ?>" required/>
                    <?php echo form_error('nama_kecamatan') ?>
                </div>
        	    <input type="hidden" name="kode_kecamatan" value="<?php echo $kode_kecamatan; ?>" /> 
        	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
        	    <a href="<?php echo site_url('kecamatan') ?>" class="btn btn-default">Cancel</a>
        	</form>
        </div>
    </div>
</section>
