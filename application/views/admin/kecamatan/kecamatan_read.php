<div class="modal fade modal-info" id="msg_info" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">
                <b>
                    <div>Info Kecamatan</div>
                </b>    
            </div>
            <div class="modal-body">
                <div id="no_alert"></div>
                <div id="kode_kecamatan_alert"></div>
                <div id="nama_kecamatan_alert"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Close</button>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->