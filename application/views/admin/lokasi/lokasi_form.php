
<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h2 class="box-title"><?php echo $button ?> Lokasi</h2>
        </div>
        <div class="box-body">
            <?php 
                if($button=='Tambah')
                    echo form_open_multipart('lokasi/create_action');
                elseif ($button=='Edit') {
                    echo form_open_multipart('lokasi/update_action');
                }
            ?>
        	    <div class="form-group">
                    <label for="varchar">Nama Lokasi <?php echo form_error('nama_lokasi') ?></label>
                    <input type="text" class="form-control" name="nama_lokasi" id="nama_lokasi" placeholder="Nama Lokasi" value="<?php echo $nama_lokasi; ?>" />
                </div>
        	    <div class="form-group">
                    <label for="foto">Foto <?php echo form_error('foto') ?></label>
                    <input type="file" name="foto" id="foto" class="filestyle" accept="image/jpeg" data-buttonText="&nbsp;Pilih Foto" 
                    data-placeholder="<?= substr($foto, 9)  ?>">
                </div>
        	    <div class="form-group">
                    <label for="fasilitas">Fasilitas <?php echo form_error('fasilitas') ?></label>
                    <textarea class="form-control" rows="3" name="fasilitas" id="fasilitas" placeholder="Fasilitas"><?php echo $fasilitas; ?></textarea>
                </div>
        	    <div class="form-group">
                    <label for="int">Kode Kecamatan <?php echo form_error('kode_kecamatan') ?></label>
                    <select class="form-control select2" id="kode_kecamatan" name="kode_kecamatan" required="true">
                        <?php
                            $data = $kode_kecamatan;
                            foreach ($kecamatan_data as $kecamatan) {
                                if(!strcmp($data, $kecamatan->kode_kecamatan)){
                                    echo "<option value='".$kecamatan->kode_kecamatan."' selected>".$kecamatan->nama_kecamatan."</option>";
                                    $ident = $kode_kecamatan;
                                }else{
                                    echo "<option value='".$kecamatan->kode_kecamatan."'>".$kecamatan->nama_kecamatan."</option>";
                                }                                   
                            }
                        ?>
                        <option value="" <?php if(($button=="Tambah")||(empty($ident))) echo "selected"; ?> >-- Pilih Kecamatan --</option>                   
                    </select>
                </div>
        	    <input type="hidden" name="kode_lokasi" value="<?php echo $kode_lokasi; ?>" /> 
        	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
        	    <a href="<?php echo site_url('lokasi') ?>" class="btn btn-default">Batal</a>
        	</form>
        </div>
    </div>
</section>

<?php
    if($this->session->flashdata('message')){
        $this->load->view("modals/modals");
    }
?>

