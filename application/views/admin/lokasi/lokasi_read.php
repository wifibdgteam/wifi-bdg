<div class="modal fade modal-info" id="msg_info" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">
                    <b>Info Lokasi</b>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4">
                        <img id="alt_foto" class="img-responsive img-rounded">
                    </div>
                    <div class="col-sm-8">
                        <div id="alt_kode_lokasi"></div>
                        <div id="alt_nama_lokasi"></div>
                        <div id="alt_kecamatan"></div>
                        <div id="alt_fasilitas"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
