<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <div class="row">
                <div class="col-md-8">
                    <h2 class="box-title">Data Lokasi</h2>
                </div>
                <div class="col-md-4 text-right">
                    <?php echo anchor(site_url('lokasi/pdf'), '<i class="fa fa-file-pdf-o"></i>', 'class="btn btn-social-icon btn-danger"'); ?>
        		    <?php echo anchor(site_url('lokasi/excel'), '<i class="fa fa-file-excel-o"></i>', 'class="btn btn-social-icon btn-success"'); ?>
                    <?php echo anchor(site_url('lokasi/create'), '<i class="fa fa-plus"></i>Tambah Lokasi', 'class="btn btn-social btn-primary"'); ?>
        	    </div>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped" id="tabelku">
                <thead>
                    <tr>
                        <th width="80px">No</th>
            		    <th>Nama Lokasi</th>
                        <th>Jumlah Wifi</th>
            		    <th>Kecamatan</th>
            		    <th>Action</th>
                    </tr>
                </thead>
    	    <tbody>
                <?php
                $start = 0;
                foreach ($lokasi_data as $lokasi)
                {
                    ?>
                    <tr>
    		    <td><?php echo ++$start ?></td>
    		    <td><?php echo $lokasi->nama_lokasi ?></td>
                <td><?php echo $lokasi->jumlah_wifi  ?></td>
    		    <td><?php echo $lokasi->nama_kecamatan ?></td>
    		    <td style="text-align:center" width="200px">
                <!-- 
                    <button id="lihat" class="btn btn-danger btn-sm" onclick="info('<?= $lokasi->kode_lokasi ?>')" title="Lihat Data"><i class="fa fa-eye"></i></button>
                 -->
                
    			<?php 
                    echo anchor(site_url('lokasi/detail_lokasi/'.$lokasi->kode_lokasi),'<i class="fa fa-eye"></i>','title="Edit Data" class="btn btn-danger btn-sm"'); 
        			echo '   '; 
        			echo anchor(site_url('lokasi/update/'.$lokasi->kode_lokasi),'<i class="fa fa-pencil"></i>','title="Edit Data" class="btn btn-danger btn-sm"'); 
        			echo '   '; 
        			echo anchor(site_url('lokasi/delete/'.$lokasi->kode_lokasi),'<i class="fa fa-trash"></i>','title="Hapus Data" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
    			?>
    		    </td>
    	        </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <?php 
            $this->load->view('admin/lokasi/lokasi_read');
        ?>

        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">

            
            $(document).ready(function () {
                $("#tabelku").dataTable();
            });


            function info(kode_lokasi) {
                $.ajax(
                    {
                        type: "POST",
                        url: baseurl+"lokasi/read",
                        data: {kode_lokasi:kode_lokasi},
                        success: function(data){
                            try {
                                var lokasi = JSON.parse(data);
                                $('#alt_foto').attr('src',baseurl+lokasi.foto);
                                $('#alt_kode_lokasi').html('Kode Lokasi : '+lokasi.kode_lokasi);
                                $('#alt_nama_lokasi').html('Nama Lokasi : '+lokasi.nama_lokasi);
                                $('#alt_kecamatan').html('Kecamatan : '+lokasi.nama_kecamatan);
                                $('#alt_fasilitas').html('Fasilitas : '+lokasi.fasilitas);
                                $('#msg_info').modal({
                                    backdrop: 'static',
                                    keyboard: false
                                });
                            } catch(e) {
                                alert(e);
                            }
                        }
                    });




                }
                // Trigger action when the contexmenu is about to be shown
        </script>
    </div>
</section>

<?php
    if($this->session->flashdata('message')){
        $this->load->view("modals/modals");
    }
?>