<!DOCTYPE html>
<html>
<head>
  <title>Report Table</title>
  <style type="text/css">
    #outtable{
      padding: 20px;
      border:0px solid #e3e3e3;
      width:auto;
      border-radius: 5px;
    }
 
    .short{
      width: 50px;
    }
 
    .normal{
      width: 100%;
    }
 
    table{
      border-collapse: collapse;
      font-family: arial;
      color:#5E5B5C;
    }
 
    thead th{
      text-align: left;
      padding: 10px;
    }
 
    tbody td{
      border-top: 1px solid #e3e3e3;
      padding: 10px;
    }
 
    tbody tr:nth-child(even){
      background: #F6F5FA;
    }
 
    tbody tr:hover{
      background: #EAE9F5
    }

    .header {
        display: block;
        width: 100%;
    }

    .footer {
        width: 100%;
        text-align: right;
        position: fixed;
    }
    .header {
        top: 0px;
    }
    .footer {
        bottom: 0px;
    }
    .pagenum:before {
        content: counter(page);
    }

  </style>
</head>
<body>
  <?php 
    $array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
    $hari = $array_hari[date("N")];
  ?>
  <div class="footer">
      <span class="pagenum"></span>
  </div>
  <div id="outtable">
    <table>
      <tr>
        <td colspan="4">
          <p class="header">
            <div style="text-align: left;">
              <b>Data Lokasi</b>
            </div>
            <div style="text-align:left;">
              <?= $hari.", ".date("d/m/Y"); ?>
            </div>
          </p>
        </td>
     </tr>
      <thead>
        <tr>
          <th class="small">No</th>
          <th class="normal">Nama Lokasi</th>
          <th class="normal">Jumlah Wifi</th>
          <th class="normal">Nama Kecamatan</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1; ?>
        <?php foreach($lokasi_data as $lokasi): ?>
          <tr>
          <td><?php echo $no; ?></td>
          <td><?php echo $lokasi->nama_lokasi ?></td>
          <td><?php echo $lokasi->jumlah_wifi  ?></td>
          <td><?php echo $lokasi->nama_kecamatan ?></td>
          </tr>
        <?php $no++; ?>
        <?php endforeach; ?>
      </tbody>
    </table>
   </div>
</body>
</html>


