<?php 
    $head = '';
    if($button=='Edit') {
        $head = 'Edit Informasi WiFi';
    } else if($button=='Tambah'){
        $head = 'Tambah Data WiFi';
    }
?>

<section class="content">

    <div class="box box-primary">
        <div class="box-header">
            <h2 class="box-title"><?= $head ?></h2>
        </div>
        <div class="box-body">                 
            <div id="map"></div><br>
            <div class="row">
                <div class="col-sm-6">
                   
                    <?php 
                        if($button=='Tambah')
                            echo form_open_multipart('wifi/create_action');
                        elseif ($button=='Edit') {
                            echo form_open_multipart('wifi/update_action');
                        }
                    ?>
                        <div class="box box-primary box-solid">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="kode_lokasi">Lokasi WiFi</label>    
                                    <select class="form-control" id="kode_lokasi" name="kode_lokasi" required="true">
                                        <?php
                                            $data1 = $kode_lokasi;
                                            foreach ($lokasi_data as $lokasi) {
                                                if(!strcmp($data1, $lokasi->kode_lokasi)){
                                                    echo "<option value='".$lokasi->kode_lokasi."' selected>".$lokasi->nama_lokasi."</option>";
                                                    $ident1 = $kode_lokasi;
                                                }else{
                                                    echo "<option value='".$lokasi->kode_lokasi."'>".$lokasi->nama_lokasi."</option>";
                                                }                                   
                                            }
                                        ?>
                                        <option value="" <?php if(($button=="Tambah")||(empty($ident1))) echo "selected"; ?> >-- Pilih Lokasi --</option>
                                    </select>

                                    <?php echo form_error('lokasi') ?>
                                </div>


                                <div class="form-group">
                                    <label for="nama_kecamatan">Kecamatan </label>
                                    <input type="text" name="nama_kecamatan" id="nama_kecamatan" required="true" class="form-control" readonly="true" 
                                    value="<?=$nama_kecamatan?>">
                                    <?php echo form_error('nama_kecamatan') ?>
                                </div>

                                <div class="form-group">
                                    <label for="fasilitas">Fasilitas </label>
                                    <textarea class="form-control" rows="4" name="fasilitas" id="fasilitas" readonly="true"><?= $fasilitas ?></textarea>
                                    <?php echo form_error('fasilitas') ?>
                                </div>

                            </div>
                        </div>
                	    
                </div>

                <div class="col-sm-6">
                    <div class="box box-solid">
                        <div class="box-body">
                            <div class="form-group">
                                    <label for="varchar">Nama WiFi</label>
                                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>" onkeyup="this.value=this.value.toUpperCase()"/>
                                    <?php echo form_error('nama') ?>
                                </div>
                                <div class="form-group">
                                    <label for="latitude">Latitude </label>
                                    <input type="text" class="form-control" rows="3" name="latitude" id="latitude" placeholder="Latitude" value="<?php echo $latitude ?>" required >
                                    <?php echo form_error('latitude') ?>
                                </div>
                                <div class="form-group">
                                    <label for="latitude">Longitude </label>
                                    <input type="text" class="form-control" rows="3" name="longitude" id="longitude" placeholder="Longitude" value="<?php echo $longitude ?>" required>
                                    <?php echo form_error('longitude') ?>
                                </div>
                                
                                
                                
                                <div class="form-group">
                                    <label for="status">Status Aktif <?php echo form_error('status') ?></label>
                                    <br>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="radio" name="status" value="AKTIF" <?php if($status=='AKTIF') echo 'checked'; ?> required>AKTIF
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="radio" name="status" value="NONAKTIF" <?php if($status=='NONAKTIF') echo 'checked'; ?> >NONAKTIF
                                        </div>
                                    </div>
                                </div>    

                                
                        </div>
                    </div>
                            <div class="text-right">
                                <input type="hidden" name="no" value="<?php echo $no; ?>" /> 
                                <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                                <a href="<?php echo site_url('admin') ?>" class="btn btn-default">Batal</a>
                            </div>
                        </form>
                </div>
            </div>

        </div>
    </div>
</section>



<script type="text/javascript">
 
    var baseurl = "<?php print base_url(); ?>";
    var map = new GMaps({
      el: '#map',
      lat: -6.914744,
      lng: 107.609810,
      height: '350px',
      width: '100%',
      fullscreenControl: true,
      zoom : 14
    });

    var marker = map.addMarker({
        lat: -6.914744,
        lng: 107.609810,
        draggable: true,
    });


    map.addControl({
      position: 'top_right',
      content: //"<div class='form-group has-feedback'>"
                    "<input type='text' name='txtcari' id='txtcari' class='form-control'  placeholder='Cari Lokasi'>",
                    //+"<span class='fa fa-search form-control-feedback'></span>"
                  //+"</div>",
      style: {
        margin: '10px',
      },
      events: {
        keypress: function(evt){
            $('#txtcari').keyup("keypress", function(e) {
                if (e.keyCode == 13) {
                    GMaps.geocode({
                      address: $('#txtcari').val().trim(),
                      callback: function(results, status){
                        if(status=='OK'){
                          var latlng = results[0].geometry.location;
                          map.removeMarkers();
                          marker = map.addMarker({
                            lat: latlng.lat(),
                            lng: latlng.lng(),
                            draggable: true,
                          });
                          $("#latitude").val(latlng.lat().toFixed(8));
                          $("#longitude").val(latlng.lng().toFixed(8));
                          map.setCenter(latlng.lat(), latlng.lng());
                          dragmarker(marker);
                        }
                      }
                    });
                }
            });
        }
      }
    });

    function dragmarker(marker){
        google.maps.event.addListener(marker, 'dragend', function (event) {
            $('#latitude').val(this.getPosition().lat().toFixed(8));
            $('#longitude').val(this.getPosition().lng().toFixed(8));
        });
    }
    

    function get_kecamatan(kode_lokasi){
        $.ajax(
            {
                type: "POST",
                url: baseurl+"wifi/get_data",
                data: {kode_lokasi:kode_lokasi},
                success: function(data){
                    try {
                        var kecamatan = JSON.parse(data);
                        $("#nama_kecamatan").val(kecamatan.nama_kecamatan);
                        $("#fasilitas").val(kecamatan.fasilitas);
                    } catch(e) {
                        console.log(e);
                    }
                }
            }
        );
    }

    $('#kode_lokasi').change(function() {
        var kode_lokasi = $('#kode_lokasi').val();
        if(kode_lokasi==""){
            $("#nama_kecamatan").val("");
            $("#fasilitas").val("");
        }else{
            get_kecamatan(kode_lokasi);
        }    
    });

    $(document).ready(function () {
        dragmarker(marker);
    });
</script>
