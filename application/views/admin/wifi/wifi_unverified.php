<?php echo $map['js']; ?>
<section class="content">
	<div class="box box-primary box-solid">
		<div class="box-header with-border">
			<h2 class="box-title">Daftar WiFi Belum Disetujui</h2>
			<div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
		</div>
		<div class="box-body">
			<div class="pull-left"> 
                <a href="<?= base_url('tambah_wifi') ?>" class="btn btn-social btn-danger" title="Tambah WiFi"><i class="fa fa-plus"></i>Tambah WiFi</a>
            </div>
            <br><br>
            <table class="table table-bordered table-striped" id="myTable">
                <thead>
                    <tr>
                        <th width="30px">No</th>
                        <th >Nama</th>
                        <th >Lokasi</th>
                        <th >Status Aktif</th>
                        <th style="text-align:center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $start = 1;
                        foreach ($wifi_data as $wifi) {
                    ?>
                    <tr>
                        <td><?= $start++ ?></td>
                        <td><?php echo strtoupper($wifi->nama) ?></td>
                        <td><?php echo strtoupper($wifi->lokasi) ?></td>
                        <td><?php echo $wifi->status ?></td>
                        <td style="text-align:center">
                            <button id="lihat" value="<?= $wifi->no ?>" class="btn btn-danger btn-sm" onclick="info('<?= $wifi->no ?>')" title="Lihat Data"><i class="fa fa-eye"></i></button>
                            <?php 
                                if($this->session->userdata('lvl')=='SUPERADMIN') {
                                    echo '   '; 
                                    echo anchor(site_url('edit/form/'.$wifi->no),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'Edit Data','class'=>'btn btn-danger btn-sm')); 
                                    echo '   '; 
                                    echo anchor(site_url('beranda/hapus_form/'.$wifi->no),'<i class="fa fa-trash"></i>','title="Hapus Data" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Apakah anda yakin ingin menghapus data ini ?\')"'); 
                                }
                            ?>
                                                  
                        </td>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
		</div>
	</div>
</section>

<?php
    //message box khusus
    $this->load->view('modals/alert/smsg_hapus.php');
    $this->load->view('modals/alert/msg_info.php');
?>

<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>

<script type="text/javascript">

    $(document).ready(function () {
        $("#myTable").dataTable();
    });
</script>