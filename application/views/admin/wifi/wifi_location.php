<?php
    $i = 0;
    foreach ($wifi_data as $wifi)
    {
        $nama_wifi[$i] = $wifi->nama;
        $latitude[$i] = $wifi->latitude;
        $longitude[$i] = $wifi->longitude;
        $foto = $wifi->foto;
        $fasilitas = $wifi->fasilitas;
        $nama_lokasi = $wifi->nama_lokasi;
        $kecamatan = $wifi->kode_kecamatan;
        $i++;
    }
?>
<section class="content">
	<div class="box box-primary">
		<div class="box-body">
			<input type="hidden" id="lokasi" value="<?= $nama_lokasi ?>">
			<div class="row" onload="get_alamat('<?= $latitude[0] ?>')">
				<div class="col-lg-3">
					<img src="<?= base_url($foto) ?>" class="img-responsive">
				</div>
				<div class="col-lg-9">
					<div class="box box-primary">
						<div class="box-header">
				        	<h3 class="box-title"><b><?= "INFORMASI LOKASI ".strtoupper($nama_lokasi) ?></b></h3>
				        </div>
				        <div class="box-body">
					        <table class="table table-striped">
					        	<tr>
					        		<th>Lokasi</th>
					        		<td><input type="text" class="form-control" name="lokasi" id="lokasii" value="<?= $nama_lokasi ?>" readonly></td>
					        	</tr>
					        	<tr>
					       			<th>Fasilitas</th>
					       			<td><textarea class="form-control" name="fasilitas" id="fasilitas" readonly><?= $fasilitas ?></textarea></td>
					        	</tr>
					        	<tr>
					        		<th>Foto</th>
					        		<td>
					        			<input type="text" name="foto" id="foto" class="form-control" value="<?= substr($foto, 9)  ?>" readonly>
				                            <?php echo form_error('foto') ?>
				                    </td>
					        	</tr>
					        	<tr>
					       			<th >Alamat</th>
					       			<td><div id="alamat"></div> </td>
					        	</tr>
					        </table>
					        <a href="<?= site_url('lokasi/update/'.$wifi->kode_lokasi) ?>" class="btn btn-social btn-danger pull-right"><i class="fa fa-edit"></i>Edit Info Lokasi</a>
					    </div>
				    </div>   
				</div>
			</div>
			
			<div class="box box-primary">
			    <div class="box-header">
			    	<h2 class="box-title"><b>DAFTAR TITIK WIFI DI <?= strtoupper($nama_lokasi) ?></b></h2>
			    	<a href="<?= base_url('wifi/create/') ?>" class="btn btn-social btn-primary pull-right"><i class="fa fa-plus"></i>Tambah Titik Wifi </a>
			    </div>
			    <div class="box-body">
				    <table class="table table-bordered table-striped" id="tabelku" >
				    	<thead>
					     	<tr>
					       		<th>Nama Wifi</th>
					       		<th>Latitude, Longitude</th>
					       	</tr>
				       	</thead>

				       	<tbody>				       	
						   	<?php
						   		for($j=0;$j<$i;$j++){
						   			if($nama_wifi[$j]!=null){
							?>
						   	<tr>
						    	<td><?= strtoupper($nama_wifi[$j])  ?></td>
						    	<td><?= $latitude[$j].','.$longitude[$j] ?></td>
						      		
						    </tr>
						    <?php
						    		}
						    	}
						    ?>
					    </tbody>
					        	
					</table>
				</div>
			</div>
			<div class="pull-right">	
			    <a href="<?= base_url('lokasi') ?>" class="btn btn_primary bg-navy">Kembali</a>
			</div>
		</div>
	</div>	
</section>

<script type="text/javascript">
	var lokasi = $('#lokasi').val();
	$(document).ready(function () {
        $("#tabelku").dataTable({
        	searching: false,
			paging: false
        });
    });

	function get_alamat(lokasi){
		var url = "https://maps.googleapis.com/maps/api/geocode/json?address="+lokasi+"&components=country:ID&key=AIzaSyCE7HqUUvAXRZDYvvpZtJ8oB-ZgddhSPE4";
	    try {
	    	$.getJSON(url, function(data, textStatus){
	    		var alamat = data.results[0].formatted_address;
	    		$('#alamat').html(': '+alamat);
	    	})
	    }catch(e){
	    	alert(e);
	    }
	}

	window.onload = get_alamat(lokasi);
</script>