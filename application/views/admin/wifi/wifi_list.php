<section class="content">
    <div class="box box-primary box-solid collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">Lihat Map</h3>   
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped" id="mytable">
                        <thead>
                            <tr>
                                <th >No</th>
                                <th >ID</th>
                                <th >Nama</th>
                                <th >Lat,Long</th>
                                <th >Detail</th>
                                <th >Lokasi</th>
                                <th style="text-align:center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $start = 1;
                            foreach ($wifi_data as $wifi)
                            {
                                ?>
                                <tr>
                                    <td><?= $start++ ?></td>
                                    <td><?php echo $wifi->no ?></td>
                                    <td><?php echo $wifi->nama ?></td>
                                    <td><a href="<?php echo 'http://maps.google.com/maps?q='.$wifi->latitude.','.$wifi->longitude ?>"><?php echo $wifi->latitude.','.$wifi->longitude ?></a></td>
                                    <td><?php echo $wifi->detail ?></td>
                                    <td><?php echo $wifi->lokasi ?></td>
                                    <td style="text-align:center">
                                    <?php 
                                    echo anchor(site_url('wifi/read/'.$wifi->no),'Lihat'); 
                                    //echo ' | '; 
                                    //echo anchor(site_url('wifi/update/'.$wifi->no),'Update'); 
                                    //echo ' | '; 
                                    //echo anchor(site_url('wifi/delete/'.$wifi->no),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                                    ?>
                                              
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
            </table>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <div class="box-title">
                Data WiFi Bandung Juara
            </div>
        </div>
        <div class="box-body">

            <table class="table table-bordered table-striped" id="mytable">
                        <thead>
                            <tr>
                                <th >No</th>
                                <th >ID</th>
                        	    <th >Nama</th>
                        	    <th >Lat,Long</th>
                        	    <th >Detail</th>
                        	    <th >Lokasi</th>
                        	    <th style="text-align:center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $start = 1;
                            foreach ($wifi_data as $wifi)
                            {
                                ?>
                                <tr>
                                    <td><?= $start++ ?></td>
                            	    <td><?php echo $wifi->no ?></td>
                           		    <td><?php echo $wifi->nama ?></td>
                                    <td><a href="<?php echo 'http://maps.google.com/maps?q='.$wifi->latitude.','.$wifi->longitude ?>"><?php echo $wifi->latitude.','.$wifi->longitude ?></a></td>
                                    <td><?php echo $wifi->detail ?></td>
                                    <td><?php echo $wifi->lokasi ?></td>
                                    <td style="text-align:center">
                                    <?php 
                                    echo anchor(site_url('wifi/read/'.$wifi->no),'Lihat'); 
                                    //echo ' | '; 
                                    //echo anchor(site_url('wifi/update/'.$wifi->no),'Update'); 
                                    //echo ' | '; 
                                    //echo anchor(site_url('wifi/delete/'.$wifi->no),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                                    ?>
                                              
                                    </td>
                            	</tr>
                            <?php
                            }
                            ?>
                        </tbody>
            </table>
        </div>
    </div>
</section>

<?php
    if($this->session->flashdata('message')){
        $this->load->view("modals/modals");
    }
?>