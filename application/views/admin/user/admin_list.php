<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h2 class="box-title">Data Admin</h2>
                </div>
                <div class="col-sm-4 text-right">
                    <a href="#" class="btn btn-social-icon btn-danger" title="Ekspor ke Word"><i class="fa fa-file-pdf-o"></i></a>
                    <a href="#" class="btn btn-social-icon btn-success" title="Ekspor ke Excel"><i class="fa fa-file-excel-o"></i></a>
                    <a href="<?= base_url('tambah_admin') ?>" class="btn btn-social btn-primary" title="Tambah admin"><i class="fa fa-plus"></i>Tambah Admin</a>
                </div>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped" id="tabelku">
                <thead>
                    <tr>
                        <th >No</th>
                        <th >Nama</th>
                        <th >Username</th>
                        <th style="text-align:center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $start = 1;
                        foreach ($user_data as $user) {
                    ?>
                        <tr>
                            <td><?= $start++ ?></td>
                            <td><?php echo $user->nama ?></td>
                            <td><?php echo $user->username ?></td>
                            <td style="text-align:center">
                                <?php 
                                    //echo anchor(site_url('wifi/read/'.$wifi->no),'Lihat'); 
                                    //echo ' | '; 
                                    //echo anchor(site_url('wifi/update/'.$wifi->no),'Update'); 
                                    //echo ' | '; 
                                    echo anchor(site_url('admin_list/hapus/'.$user->username),'<i class="fa fa-trash"></i>','title="Hapus Data" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                                ?>                    
                            </td>
                        </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
            <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
            <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
            <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $("#tabelku").dataTable();
                });
            </script>
        </div>
    </div>
</section>