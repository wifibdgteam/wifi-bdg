<?php 
    $head = '';
    if($button=='Edit') {
        $head = 'Edit Informasi WiFi';
    } else if($button=='Tambah'){
        $head = 'Tambah Administrator';
    }
?>
<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h2 class="box-title"><?= $head ?></h2>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                   
                    <?php 
                        if($button=='Tambah')
                            echo form_open_multipart('tambah_admin/aksi');
                        elseif ($button=='Edit') {
                            echo form_open_multipart('edit_admin/aksi');
                        }
                    ?>

                	    <div class="form-group">
                            <label for="varchar">Nama Admin</label>
                            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Admin" value="<?php echo $nama; ?>" required <?php if($button=='Edit') echo "readonly"; ?>/>
                            <?php echo form_error('nama') ?>
                        </div>
                	    <div class="form-group">
                            <label for="username">Username </label>
                            <input type="text" class="form-control" rows="3" name="username" id="username" placeholder="Username" value="<?php echo $username ?>" required>
                            <?php echo form_error('username') ?>
                        </div>
                        <div class="form-group">
                            <label for="password">Password </label>
                            <input type="password" class="form-control" rows="3" name="password" id="password" placeholder="Password" value="<?php echo $password ?>" required>
                            <?php echo form_error('password') ?>
                        </div>
                	    <div class="form-group">
                            <label for="konfirmasi">Konfirmasi Password </label>
                            <input type="password" class="form-control" name="konfirmasi" id="konfirmasi" placeholder="Konfrmasi Password" required="">
                        </div>
                        
                	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                	    <a href="<?php echo site_url('admin_list') ?>" class="btn btn-default">Batal</a>
                	</form>
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">
    
    function ordinat(lat,long) {
        $('#username').val(lat.toFixed(8));
        $('#password').val(long.toFixed(8));
    }

</script>
