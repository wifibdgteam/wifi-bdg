<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo base_url('img/icon.ico') ?>">

    <title>Sistem Informasi Geografis Wifi Bandung Juara</title>

    <?php $this->load->view('template_user/css.php'); ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCE7HqUUvAXRZDYvvpZtJ8oB-ZgddhSPE4"></script>
  	<script src="<?= base_url('vendor/solid/assets/js/gmaps.js') ?>"></script>
	
	  <style type="text/css">
	    #map {
	      width: 99%;
	      height: 600px;
	    }
	  </style>

</head>
<body>
	<?php $this->load->view('template_user/header.php'); ?>
	<?php $this->load->view('template_user/content.php'); ?>

	<?php $this->load->view('template_user/footer.php'); ?>

	
	<?php $this->load->view('template_user/javascript.php'); ?>

</body>
</html>