<div class="modal fade modal-success" id="message" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">
                <b><i class="icon fa fa-check"></i>
                    Alert !
                </b>    
            </div>
            <div class="modal-body">
                <?php  
                    if($this->session->flashdata('smessage')) {
                        echo $this->session->flashdata('smessage');
                    }
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Close</button>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->