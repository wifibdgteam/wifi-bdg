

<div class="modal fade modal-danger" tabindex="-1" id="message" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">
            <b><i class="icon glyphicon glyphicon-ban-circle"></i>
                Alert !
            </b>
        </h4>
      </div>
      <div class="modal-body">
        <?php  
            if($this->session->flashdata('fmessage')) {
                echo $this->session->flashdata('fmessage');
            }
        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
