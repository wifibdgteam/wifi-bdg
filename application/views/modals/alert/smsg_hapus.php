<div class="modal fade modal-success" id="smsg_hapus" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">
                <b><i class="icon fa fa-check"></i>
                    Alert !
                </b>    
            </div>
            <div class="modal-body">
                Data Berhasil dihapus
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-right" data-dismiss="modal" onclick="window.location.reload();">Close</button>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->