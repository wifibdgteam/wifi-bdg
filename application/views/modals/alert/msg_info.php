<div class="modal fade modal-info" id="msg_info" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <h4 class="modal-title">
                <b>
                    <div id="nama_alert1"></div>
                </b>    
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-5">
                        <img id="foto_alert" src="" class="img-responsive img-rounded">
                    </div>
                    <div class="col-sm-7">
                        <div id="no_alert"></div>
                        <div id="nama_alert2"></div>
                        <div id="lokasi_alert"></div>
                        <div id="latlong_alert"></div>
                        <div id="fasilitas_alert"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Tutup</button>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->