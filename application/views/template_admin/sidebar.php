<!-- Left side column. contains the sidebar -->
<?php
    $now = $this->uri->segment(1); 
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
          <div class="pull-left ">
            <img src="<?= base_url('img/icon.png') ?> " class="img-circle " alt="User Image"  width="50px" height="50px">
          </div>
          <div class="pull-left info">
            <p><?= $this->session->userdata('name') ?></p>

            <a href="#"><i class="fa fa-circle text-success"></i> <?= $this->session->userdata('uname') ?></a>
          </div>
        </div><!-- /.user-panel -->
        <ul class="sidebar-menu">
            <li class="header">NAVIGASI UTAMA</li>
            <li class="<?php if($now=='wifi') echo 'active'; ?>"><a href="<?=  base_url('wifi') ?>"><i class="fa fa-wifi"></i><span>DATA WIFI</span></a></li>
            <li class="<?php if($now=='lokasi') echo 'active'; ?>"><a href="<?=  base_url('lokasi') ?>"><i class="glyphicon glyphicon-record"></i><span>DATA LOKASI</span></a></li>
            <li class="<?php if($now=='kecamatan') echo 'active'; ?>"><a href="<?=  base_url('kecamatan') ?>"><i class="fa fa-university"></i><span>DATA KECAMATAN</span></a></li>
            

            <!-- 
            
            <li class="<?php if($now=='admin_list') echo 'active'; ?>"><a href="<?=  base_url('admin_list') ?>"><i class="fa fa-users"></i><span>DATA ADMIN</span></a></li>
            
            -->
            

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
