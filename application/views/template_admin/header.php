
<header class="main-header">
    <!--
    <div class="bg-aqua color-palette text-black">
      <div style="padding: 10px">
        <div class="row" >
          <div class="col-md-1">
            <a href="<?= base_url('beranda') ?>">
              <img class="img-responsive img-rounded" src="<?= base_url('img/icon.png') ?>">
            </a>
          </div>
          <div class="col-md-10">
            <h3><b>GIS Lokasi WiFi Kota Bandung</b></h3>
            <h4>Selamat Bekerja <?= $this->session->userdata('name'); ?> </h4>
          </div>
          <div class="col-md-1">
            <img class="img-responsive img-rounded" src="<?= base_url('img/logobandung.png') ?>">
          </div>
        </div>
      </div>  
    </div>
    -->
   <!-- Logo -->
    <a href="<?= base_url('wifi') ?>" class="logo"><b>WiFi</b> Bandung</a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs"><?= $this->session->userdata('name') ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-body bg-red">
                            <?= $this->session->userdata('name') ?> - <?= $this->session->userdata('uname') ?>
                        </li>
                        <li class="user-footer bg-red">
                            <div class="pull-right">
                                <a href="<?= base_url('admin/logout') ?>" class="btn btn-default btn-flat">Keluar</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>