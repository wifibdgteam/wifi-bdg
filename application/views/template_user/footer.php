	 <div id="footerwrap">
	 	<div class="container">
		 	<div class="row">
		 		<div class="col-lg-4">
		 			<h4>Tentang</h4>
		 			<div class="hline-w"></div>
		 			<p>Aplikasi ini dibuat untuk mempermudah masyarakat mengetahui informasi lokasi wifi Bandung Juara yang tersedia di Kota Bandung </p>
		 		</div>
		 		
		 		<div class="col-lg-4">
		 			<h4>Kontak</h4>
		 			<div class="hline-w"></div>
		 			<p>
		 				PEMERINTAH KOTA BANDUNG<br/>
		 				DINAS KOMUNIKASI DAN INFORMATIKA<br/>
		 				Jalan Wastukencana No. 2, Bandung, Jawa Barat 40117<br>
		 				Telp. 022 4230393, 022 4230097
		 			</p>
		 		</div>
		 		<div class="col-lg-4" style="padding-top: 50px;">
		 			<img src="<?= base_url('img/diskominfo.png') ?>" class="img-responsive">
		 		</div>
		 	
		 	</div>
	 	</div>
	 </div>