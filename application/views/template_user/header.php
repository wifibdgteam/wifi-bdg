<!-- Fixed navbar -->
<?php

  $url = $this->uri->segment(2);
?>
    <div class="navbar navbar-default" role="navigation" style="border: 0px; border-radius: 0px; ">
      <div class="container">
        <div class="row"> 
            <div class="col-sm-2 col-lg-1">
              <div class="img-responsive gambar1"><img src="<?= base_url('img/logobandung.png')?>"></div>
            </div>
            <div class="col-sm-2 col-lg-1">
              <div class="img-responsive gambar"><img src="<?= base_url('img/icon.png')?>"></div>
            </div>
            <div class="col-sm-2 col-lg-5">
                <a class='navbar-brand ' href="<?= base_url('user') ?>" style="color: white;">
                  SISTEM INFORMASI GEOGRAFIS <br>WIFI BANDUNG JUARA
                </a>  
            </div>
                  
                
            <div class="col-sm-6 col-lg-5">
                <button type="button" class="navbar-toggle img-responsive" data-toggle="collapse" data-target=" .navbar-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <div class="navbar navbar-collapse collapse navbar-right">
                  <ul class="nav navbar-nav">
                    <li <?php if(!$url) echo "class='active'"; ?>><a href="<?= base_url('user') ?>">BERANDA</a></li>
                    <li <?php if($url=="daftar_lokasi") echo "class='active'"; ?>><a href="<?= base_url('user/daftar_lokasi') ?>">DAFTAR LOKASI TERPASANG WIFI</a></li>
                  </ul>
                </div><!--/.nav-collapse -->
            </div>

        </div>
      </div>
    </div>