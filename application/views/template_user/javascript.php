	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script src="<?= base_url('vendor/solid/assets/js/bootstrap.min.js') ?>"></script>
	<script src="<?= base_url('vendor/solid/assets/js/retina-1.1.0.js') ?>"></script>
	<script src="<?= base_url('vendor/solid/assets/js/jquery.hoverdir.js') ?>"></script>
	<script src="<?= base_url('vendor/solid/assets/js/jquery.hoverex.min.js') ?>"></script>
	<script src="<?= base_url('vendor/solid/assets/js/jquery.prettyPhoto.js') ?>"></script>
  	<script src="<?= base_url('vendor/solid/assets/js/jquery.isotope.min.js') ?>"></script>
  	<script src="<?= base_url('vendor/solid/assets/js/custom.js') ?>"></script>
  	<script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
	<!-- FastClick -->
	<script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.min.js'); ?>"></script>

	<script type="text/javascript">var baseurl = "<?php print base_url(); ?>";</script>

	<script>
	    $(function () {

	        $('#mytable').DataTable({
	            "paging": true,
	            "lengthChange": false,
	            "searching": true,
	            "ordering": true,
	            "info": true,
	            "autoWidth": false
	        });

	    });
	   
	</script>