<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class auth {

    public function cek_auth() {

		$this->ci =& get_instance();
		$this->sesiadmin  = $this->ci->session->userdata('isLoginAdmin');
		$this->hak = $this->ci->session->userdata('lvl');
		if($this->sesiadmin != TRUE){
			$this->ci->session->set_flashdata('fmessage', 'Silahkan Login Terlebih Dahulu!');
			redirect('admin','refresh');
			exit();
		}
	}

	public function hak_akses($user="") {	

    	if($this->hak<>$user){ 
    		$this->ci->session->set_flashdata('fmessage', 'Anda tidak berhak mengakses halaman ini!');
    		redirect('beranda');
    	}elseif ($this->hak=="") {
    		$this->ci->session->set_flashdata('fmessage', 'Anda belum login!');
    		redirect('admin');
    	}else{
 
    	}
	}

}
 
?>